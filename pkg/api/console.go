package api

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net"
	"net/http"
	"net/url"
	"os"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/gosuri/uiprogress"
	dac "github.com/xinsnake/go-http-digest-auth-client"
)

var (
	progressCtrl      *uiprogress.Progress
	maxProgressErrors int = 5
)

func (console *Console) CreateRequestUrl(path string, params map[string]string) *url.URL {
	u := url.URL{}
	u.Scheme = "http"
	u.Host = console.Hostname
	u.Path = path
	u.RawPath = strings.ReplaceAll(path, "+", "%2B")
	q := u.Query()
	for key, val := range params {
		q.Set(key, val)
	}
	u.RawQuery = q.Encode()

	return &u
}

func (console *Console) createMultipartRequest(reqURL *url.URL, reader io.Reader, params map[string]string) (*dac.DigestRequest, error) {
	reqbody := &bytes.Buffer{}
	writer := multipart.NewWriter(reqbody)
	part, err := writer.CreateFormFile("file", "dummy")
	if err != nil {
		return nil, err
	}

	_, err = io.Copy(part, reader)
	for key, val := range params {
		_ = writer.WriteField(key, val)
	}
	err = writer.Close()
	if err != nil {
		return nil, err
	}

	req := dac.NewRequest(console.User, console.Password, "POST", reqURL.String(), reqbody.String())

	req.HTTPClient = &http.Client{
		Timeout: 180 * time.Second,
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())
	return &req, nil
}

func (console *Console) Upload(filename string, uri string, params map[string]string) error {
	var (
		reader *os.File
		resp   *http.Response
		err    error
	)

	reader = os.Stdin
	if filename != "" && filename != "-" {
		reader, err = os.Open(filename)

		if err != nil {
			return err
		}
		defer reader.Close()
	}

	reqURL := console.CreateRequestUrl(uri, params)
	req, err := console.createMultipartRequest(reqURL, reader, nil)
	if err != nil {
		return err
	}

	resp, err = req.Execute()
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return fmt.Errorf("Upload failed: %d", resp.StatusCode)
	}
	return nil
}

func (console *Console) Reset(rtype string) error {

	reqURL := console.CreateRequestUrl(fmt.Sprintf("/reset/%s", rtype), nil)
	req := dac.NewRequest(console.User, console.Password, "GET", reqURL.String(), "")
	resp, err := req.Execute()
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return fmt.Errorf("reset failed: %d", resp.StatusCode)
	}
	return nil
}

func (console *Console) Cleanup() error {

	reqURL := console.CreateRequestUrl("/cleanup", nil)
	req := dac.NewRequest(console.User, console.Password, "GET", reqURL.String(), "")
	resp, err := req.Execute()
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return fmt.Errorf("cleanup failed: %d", resp.StatusCode)
	}
	return nil

}

func (console *Console) Download(uri string, filename string) error {
	var (
		writer *os.File
		err    error
	)
	writer = os.Stdout
	if filename != "" && filename != "-" {
		writer, err = os.OpenFile(filename, os.O_CREATE|os.O_RDWR|os.O_TRUNC, os.FileMode(0644))
		if err != nil {
			return err
		}
		defer writer.Close()
	}

	reqURL := console.CreateRequestUrl(uri, nil)

	//fmt.Fprintf(os.Stderr, "%s\n", reqURL.String())

	req := dac.NewRequest(console.User, console.Password, "GET", reqURL.String(), "")
	resp, err := req.Execute()
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return fmt.Errorf("download failed for %s: %d", uri, resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	_, err = writer.Write(body)
	if err != nil {
		return err
	}

	return nil
}

func (console *Console) List(result interface{}, uri string, params map[string]string) error {
	reqURL := console.CreateRequestUrl(uri, params)
	req := dac.NewRequest(console.User, console.Password, "GET", reqURL.String(), "")
	resp, err := req.Execute()
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return fmt.Errorf("list failed for %s %v: %d", uri, params, resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	json.Unmarshal(body, &result)
	return nil
}

func (console *Console) Invoke(uri string, params map[string]string) error {
	reqURL := console.CreateRequestUrl(uri, params)
	req := dac.NewRequest(console.User, console.Password, "GET", reqURL.String(), "")
	resp, err := req.Execute()
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return fmt.Errorf("invoke failed for %s %v: %d", uri, params, resp.StatusCode)
	}
	return nil

}

func (console *Console) GetRaw(uri string, params map[string]string) ([]byte, error) {
	reqURL := console.CreateRequestUrl(uri, params)
	req := dac.NewRequest(console.User, console.Password, "GET", reqURL.String(), "")
	resp, err := req.Execute()
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return nil, fmt.Errorf("invoke failed for %s %v: %d", uri, params, resp.StatusCode)
	}
	return ioutil.ReadAll(resp.Body)

}

func (console *Console) RequestAsync(out chan interface{}, t reflect.Type, uri string, params map[string]string) {
	reqURL := console.CreateRequestUrl(uri, params)
	req := dac.NewRequest(console.User, console.Password, "GET", reqURL.String(), "")
	resp, err := req.Execute()
	if err != nil {
		out <- err
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		out <- fmt.Errorf("list failed for %s %v: %d", uri, params, resp.StatusCode)
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		out <- err
		return
	}

	result := reflect.New(t).Interface()
	json.Unmarshal(body, &result)
	out <- result
}

func (console *Console) RequestMD5Checksum(out chan MD5ChecksumResponse, uri string, ref *string) {
	reqURL := console.CreateRequestUrl(uri, nil)
	RequestMD5Checksum(out, reqURL.String(), ref, console)
}

func RequestMD5Checksum(out chan MD5ChecksumResponse, uri string, ref *string, console *Console) {
	result := MD5ChecksumResponse{}
	result.URI = uri
	var req dac.DigestRequest
	if console != nil {
		req = dac.NewRequest(console.User, console.Password, "GET", uri, "")
	} else {
		req = dac.NewRequest("", "", "GET", uri, "")
	}
	resp, err := req.Execute()
	if err != nil {
		result.Err = err
		out <- result
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode == 404 {
		result.Checksum = "-"
		out <- result
		return
	}

	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		result.Err = fmt.Errorf("list failed for %s: %d", uri, resp.StatusCode)
		out <- result
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		result.Err = err
		out <- result
		return
	}

	result.Checksum = strings.TrimSpace(string(body))
	*ref = result.Checksum
	out <- result
}

func (console *Console) ShowProgress(name string) (int, error) {
	var progress int64 = 0
	var wg sync.WaitGroup
	var lastError error
	var errCount int = 0

	bar := progressCtrl.AddBar(100).AppendCompleted().PrependFunc(func(b *uiprogress.Bar) string {
		return fmt.Sprintf("%-6s", name)
	})
	bar.Width = 50
	wg.Add(1)

	go func() {
		defer wg.Done()
		for {
			body, err := console.GetRaw("/progress", nil)
			if err != nil {
				errCount = errCount + 1
				if errCount > maxProgressErrors {
					lastError = err
					return
				} else {
					continue
				}
			}

			s := string(body)
			if strings.Index(s, "ERROR") != -1 {
				lastError = fmt.Errorf("%s", s)
				return
			}

			progress, err = strconv.ParseInt(strings.TrimSpace(s), 10, 64)
			if err != nil {
				lastError = err
				return
			}

			bar.Set(int(progress))
			if progress == 100 {
				break
			}
			time.Sleep(100 * time.Millisecond)
		}
	}()
	wg.Wait()
	return errCount, lastError
}

func (console *Console) Ping() bool {
	timeout := 1000 * time.Millisecond
	_, err := net.DialTimeout("tcp", console.Hostname+":80", timeout)
	if err != nil {
		return false
	}
	return true
}

func (console *Console) GetConsoleServerState() (string, error) {
	body, err := console.GetRaw("/console_server_running", nil)
	if err != nil {
		return "", err
	}
	if strings.Index(string(body), "yes") == 0 {
		return "Running", nil
	}
	return "Stopped", nil
}

func (console *Console) StartConsoleServer() error {
	return console.Invoke("/console_server_start", nil)
}

func (console *Console) StopConsoleServer() error {
	return console.Invoke("/console_server_start", nil)
}

func (console *Console) GetTestdata() ([]byte, error) {
	body, err := console.GetRaw("/testdata", nil)
	if err != nil {
		return nil, err
	}

	inp := strings.ReplaceAll(strings.ReplaceAll(string(body), " ", ""), "\n", "")
	buffer, err := hex.DecodeString(inp)
	if err != nil {
		return nil, err
	}
	if len(buffer) != 51 {
		return nil, fmt.Errorf("Expecting %d bytes of testdata", 51)
	}
	return buffer, nil
}

func (console *Console) GetSysinfo(full bool) (*Sysinfo, error) {
	var err error
	result := Sysinfo{}
	if full {
		err = console.List(&result, "/sysinfo", map[string]string{"full": "true"})
	} else {
		err = console.List(&result, "/sysinfo", nil)
	}
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func ShowProgressBegin() {
	progressCtrl = uiprogress.New()
	progressCtrl.SetOut(os.Stdout)
	progressCtrl.Start()
}

func ShowProgressFinish() {
	progressCtrl.Stop()
}
