package api

var (
	Version = "dev"
	Commit  = "none"
	Date    = "unknown"
)

type Console struct {
	Name     string
	Hostname string
	User     string
	Password string
}

type Font struct {
	Name   string `json:"name"`
	Active bool   `json:"active"`
}

type FontList struct {
	Fonts []Font `json:"fonts"`
}

type Gamma struct {
	Name   string `json:"name"`
	Active bool   `json:"active"`
}

type GammaList struct {
	Gammas []Gamma `json:"gammas"`
}

type Configuration struct {
	WiFi struct {
		SSID     string `json:"ssid" yaml:"ssid"`
		Password string `json:"password" yaml:"password"`
		Hostname string `json:"hostname" yaml:"hostname"`
	} `json:"wifi" yaml:"wifi"`
	Firmware struct {
		Server  string `json:"server" yaml:"server"`
		Path    string `json:"path" yaml:"path"`
		Version string `json:"version" yaml:"version"`
	} `json:"firmware" yaml:"firmware"`
	HTTPUser struct {
		Name      string `json:"name" yaml:"name"`
		Password  string `json:"password" yaml:"password"`
		Protected string `json:"protected" yaml:"protected"`
	} `json:"httpUser" yaml:"httpUser"`
	StaticIP struct {
		Address   string `json:"address" yaml:"address"`
		Gateway   string `json:"gateway" yaml:"gateway"`
		Netmask   string `json:"netmask" yaml:"netmask"`
		DNSServer string `json:"dnsserver" yaml:"dnsserver"`
	} `json:"staticIp" yaml:"staticIp"`
	Scanlines struct {
		Active    bool `json:"active" yaml:"active"`
		Odd       bool `json:"odd" yaml:"odd"`
		Thick     bool `json:"thick" yaml:"thick"`
		Intensity int  `json:"intensity" yaml:"intensity"`
		Dopre     bool `default:"true" json:"dopre" yaml:"dopre"`
	} `json:"scanlines" yaml:"scanlines"`
	OSD struct {
		Font         string `json:"font" yaml:"font"`
		SettingsMode int    `json:"settingsMode" yaml:"settingsMode"`
	} `json:"osd" yaml:"osd"`
	Video struct {
		Resolution         string `json:"resolution" yaml:"resolution"`
		Colorspace         int    `json:"colorspace" yaml:"colorspace"`
		Deinterlacer       string `json:"deinterlacer" yaml:"deinterlacer"`
		DeinterlacerMode   int    `json:"deinterlacerMode" yaml:"deinterlacerMode"`
		ColorExpansionMode int    `json:"colorExpansionMode" yaml:"colorExpansionMode"`
		GammaMode          string `json:"gammaMode" yaml:"gammaMode"`
		ForceMode          int    `json:"forceMode" yaml:"forceMode"`
		AutoResetAlignment bool   `json:"autoResetAlignment" yaml:"autoResetAlignment"`
		EnableHQ2x         bool   `json:"enableHq2x" yaml:"enableHq2x"`
		HQ2xStage1         bool   `json:"hq2xStage1" yaml:"hq2xStage1"`
		NoAdvIntOnClkChg   bool   `json:"noAdvIntOnClkChg" yaml:"noAdvIntOnClkChg"`
		Scaler             struct {
			Hscale []byte `json:"-" yaml:",flow"`
			Vscale []byte `json:"-" yaml:",flow"`
		} `json:"scaler" yaml:"scaler"`
	} `json:"videoconfig" yaml:"videoconfig"`
	CaptureAdjust struct {
		ManualX  bool `json:"manualx" yaml:"manualx"`
		ManualY  bool `json:"manualy" yaml:"manualy"`
		X        int  `json:"x" yaml:"xVal"`
		Y        int  `json:"y" yaml:"yVal"`
		BorderX  int  `json:"borderX" yaml:"borderX"`
		BorderY  int  `json:"borderY" yaml:"borderY"`
		Border2X int  `json:"border2X" yaml:"border2X"`
		Border2Y int  `json:"border2Y" yaml:"border2Y"`
	} `json:"captureAdjust" yaml:"captureAdjust"`
	Misc struct {
		ExtraPins   int `json:"extraPins" yaml:"extraPins"`
		DoorHandler struct {
			Mode int `json:"mode" yaml:"mode"`
		} `json:"doorHandler" yaml:"doorHandler"`
		PsNeeInjectType int  `json:"psneeInjectType" yaml:"psneeInjectType"`
		EndlessMenu     bool `json:"endlessMenu" yaml:"endlessMenu"`
		CECDisable      bool `json:"cecDisable" yaml:"cecDisable"`
		SendData        bool `json:"sendData" yaml:"sendData"`
	} `json:"misc" yaml:"misc"`
	ConsoleServer struct {
		Enabled  bool   `json:"enabled" yaml:"enabled"`
		Port     int    `json:"port" yaml:"port"`
		Baudrate int    `json:"baudrate" yaml:"baudrate"`
		WordBits int    `json:"wordBits" yaml:"wordBits"`
		Parity   string `json:"parity" yaml:"parity"`
		StopBits string `json:"stopBits" yaml:"stopBits"`
	} `json:"consoleServer" yaml:"consoleServer"`
}

type ActiveConsoleConfig struct {
	Model           string `json:"fw_model"`
	Version         string `json:"fw_version"`
	FirmwareServer  string `json:"firmware_server"`
	FirmwarePath    string `json:"firmware_server_path"`
	FirmwareVersion string `json:"firmware_version"`
}

type MD5ChecksumResponse struct {
	URI      string
	Err      error
	Checksum string
}

type Sysinfo struct {
	IsDemoData    bool
	SDKVersion    string `json:"sdk_version" yaml:"sdkversion"`
	CPUFreqMHz    int64  `json:"cpu_freq_mhz"`
	I2CErrorCount int64  `json:"i2c_error_count"`
	FS            struct {
		Total int64 `json:"total" yaml:"total"`
		Free  int64 `json:"free"`
		Used  int64 `json:"used"`
	} `json:"fs"`
	Memory struct {
		Sketch struct {
			Total int64 `json:"total"`
			Free  int64 `json:"free"`
			Used  int64 `json:"used"`
		} `json:"sketch_space"`
		Heap struct {
			Total int64 `json:"total"`
			Free  int64 `json:"free"`
			Min   int64 `json:"min"`
		} `json:"heap"`
	} `json:"memory"`
	Wifi struct {
		MAC struct {
			Station     string `json:"sta"`
			AccessPoint string `json:"ap"`
		} `json:"mac"`
		Network struct {
			Address   string `json:"address"`
			Gateway   string `json:"gateway"`
			Netmask   string `json:"netmask"`
			DNSServer string `json:"dnsserver"`
		} `json:"net"`
		RSSI    int64 `json:"rssi"`
		Quality int64 `json:"quality"`
		Channel int64 `json:"channel"`
	} `json:"wifi"`
	Chip struct {
		Type     string `json:"type"`
		Cores    int64  `json:"cores"`
		Revision int64  `json:"revision"`
		Features int64  `json:"features"`
	} `json:"chip"`
	CurrentVideo struct {
		ResolutionData   int64  `json:"resolution_data"`
		Resolution       int64  `json:"resolution"`
		CaptureAdjustX   int64  `json:"capture_adjust_x"`
		CaptureAdjustY   int64  `json:"capture_adjust_y"`
		DeinterlacerMode int64  `json:"deinterlacer_mode"`
		OSDResolution    string `json:"osd_resolution"`
		ColorExpansion   int64  `json:"color_expansion"`
		GammaMode        int64  `json:"gamma_mode"`
	} `json:"current"`
	Firmware struct {
		Model   string `json:"model"`
		Version string `json:"version"`
	} `json:"fw"`
	Testdata string `json:"testdata"`
}
