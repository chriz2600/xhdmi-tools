package api

import (
	"bufio"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io"
	"math"
	"os"
)

const (
	maxAddr    = 3072
	charWidth  = 8
	charHeight = 16
)

func CreateFontData(inputName string, outputName string, newVersion bool) error {
	var (
		err    error
		reader *os.File
		writer *os.File
	)

	reader = os.Stdin
	if inputName != "" && inputName != "-" {
		reader, err = os.Open(inputName)
		if err != nil {
			return err
		}
		defer reader.Close()
	}

	writer = os.Stdout
	if outputName != "" && outputName != "-" {
		writer, err = os.OpenFile(outputName, os.O_RDWR|os.O_CREATE|os.O_TRUNC, os.FileMode(0644))
		if err != nil {
			return err
		}
		defer writer.Close()
	}

	err = CreateFontDataFromFile(reader, writer, newVersion)
	return err
}

func CreateFontDataFromFile(input *os.File, fontfile *os.File, newVersion bool) error {
	img, err := png.Decode(bufio.NewReader(input))
	if err != nil {
		return err
	}

	fontData := CreateFontDataFromImage(img)
	fontData = append(fontData, make([]byte, 4096-len(fontData))...)

	// Convert data to v2 format
	if newVersion {
		fontDataV2 := make([]byte, len(fontData))
		split := 128

		t := 0
		for p := 0; p < split; p++ {
			for y := 0; y < 16; y++ {
				fontDataV2[t] = fontData[(p*16)+y]
				fontDataV2[t+1] = fontData[((p+split)*16)+y]
				t = t + 2
			}
		}

		fontfile.Write(fontDataV2)
	} else {
		fontfile.Write(fontData)
	}

	return nil
}

func CreateFontDataFromImage(img image.Image) []byte {
	width := img.Bounds().Max.X
	height := img.Bounds().Max.Y
	charsPerLine := width / charWidth
	offset := 0

	// fmt.Fprintf(os.Stderr, "input image size: %dx%d\n", width, height)
	// cm := img.ColorModel()
	// var model string
	// switch cm {
	// case color.RGBAModel:
	// 	model = "RGBA"
	// 	break
	// case color.RGBA64Model:
	// 	model = "RGBA64"
	// 	break
	// case color.YCbCrModel:
	// 	model = "YCbCr"
	// 	break
	// case color.Alpha16Model:
	// 	model = "Alpha16"
	// 	break
	// case color.CMYKModel:
	// 	model = "CMYK"
	// 	break
	// case color.Gray16Model:
	// 	model = "Gray16"
	// 	break
	// case color.GrayModel:
	// 	model = "Gray"
	// 	break
	// case color.NRGBA64Model:
	// 	model = "NRGBA64"
	// 	break
	// case color.NRGBAModel:
	// 	model = "NRGBA"
	// 	break
	// case color.NYCbCrAModel:
	// 	model = "NYCbCrA"
	// 	break
	// default:
	// 	model = "unknown"
	// 	break
	// }
	// fmt.Fprintf(os.Stderr, "color model: %s\n", model)

	var out byte
	var data []byte
	for addr := 0; addr < maxAddr; addr++ {
		xbase := int(math.Floor(float64(addr/charsPerLine))*charWidth) % width
		y := (addr % charHeight) + (int(math.Floor(float64(addr)/math.Floor(charHeight*float64(charsPerLine)))) * charHeight)
		for pxl := 0; pxl < charWidth; pxl++ {
			x := xbase + pxl
			c := color.GrayModel.Convert(img.At(x, y)).(color.Gray)
			if c.Y > 0 {
				out = out | (1 << (7 - pxl))
			}
		}
		data = append(data, out)
		out = 0
		if (addr + offset) == (width*height/charWidth)-1 {
			break
		}
	}
	return data
}

func CreateImageFromFontData(inputName string, outputName string, newVersion bool) error {
	var (
		err    error
		reader *os.File
		writer *os.File
	)

	reader = os.Stdin
	if inputName != "" && inputName != "-" {
		reader, err = os.Open(inputName)
		if err != nil {
			return err
		}
		defer reader.Close()
	}

	writer = os.Stdout
	if outputName != "" && outputName != "-" {
		writer, err = os.OpenFile(outputName, os.O_RDWR|os.O_CREATE|os.O_TRUNC, os.FileMode(0644))
		if err != nil {
			return err
		}
		defer writer.Close()
	}

	img := CreateImageFromFontDataFile(reader, newVersion)
	err = png.Encode(writer, img)

	return err
}

func CreateImageFromFontDataFile(input *os.File, newVersion bool) image.Image {
	stat, _ := input.Stat()
	fsize := stat.Size()
	return CreateImageFromFontDataReader(bufio.NewReader(input), fsize, newVersion)
}

func CreateImageFromFontDataReader(reader io.ByteReader, fsize int64, newVersion bool) image.Image {

	columns := 16
	rowHeight := 16
	rows := int(math.Ceil(float64(fsize) / float64(columns) / float64(rowHeight)))
	iwidth := columns * 8
	iheight := rows * rowHeight

	r := image.Rectangle{}
	r.Min.X = 0
	r.Min.Y = 0
	r.Max.X = iwidth
	r.Max.Y = iheight

	xoffset := 0
	yoffset := 0

	img := image.NewGray(r)

	if newVersion {
		if fsize != 4096 {
			fmt.Fprintf(os.Stderr, "Error: v2 font data has to exactly 4096 bytes long")
			return img
		}
	}

	if newVersion {
		b1, err := reader.ReadByte()
		b2, err := reader.ReadByte()
		for err == nil {
			y := 0
			for y < 16 {
				x := 0
				for x < 8 {
					if (b1>>(7-x))&1 == 1 {
						img.SetGray(xoffset+x, yoffset+y, color.Gray{Y: 255})
					}
					if (b2>>(7-x))&1 == 1 {
						img.SetGray(xoffset+x, yoffset+y+128, color.Gray{Y: 255})
					}
					x = x + 1
				}
				b1, err = reader.ReadByte()
				b2, err = reader.ReadByte()
				y = y + 1
			}
			xoffset = xoffset + 8
			if xoffset >= iwidth {
				yoffset = yoffset + 16
				xoffset = 0
			}
		}
	} else {
		b, err := reader.ReadByte()
		for err == nil {
			y := 0
			for y < 16 {
				x := 0
				for x < 8 {
					if (b>>(7-x))&1 == 1 {
						img.SetGray(xoffset+x, yoffset+y, color.Gray{Y: 255})
					}
					x = x + 1
				}
				b, err = reader.ReadByte()
				y = y + 1
			}
			xoffset = xoffset + 8
			if xoffset >= iwidth {
				yoffset = yoffset + 16
				xoffset = 0
			}
		}
	}

	return img
}

func (console *Console) DownloadFont(name string, filename string) error {
	return console.Download(fmt.Sprintf("/data/%s.data", name), filename)
}

func (console *Console) UploadFont(filename string, name string) error {
	return console.Upload(filename, "/upload/font", map[string]string{"name": name})
}

func (console *Console) ListFonts() (*FontList, error) {
	result := FontList{}
	err := console.List(&result, "/font/list", nil)
	return &result, err
}

func (console *Console) DeleteFont(name string) error {
	return console.Invoke("/delete/font", map[string]string{"name": name})
}

func (console *Console) LoadFont(name string) error {
	return console.Invoke("/font/load", map[string]string{"name": name})
}
