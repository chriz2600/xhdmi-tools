package cmd

import (
	"bufio"
	"fmt"
	"os"
	"path"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/xhdmi-tools/pkg/api"
	"gopkg.in/yaml.v3"
)

var (
	consoleHostname string
	consoleUser     string
	consolePassword string

	rootCmd = &cobra.Command{
		Use:   path.Base(os.Args[0]),
		Short: fmt.Sprintf("XHDMI tools (version %s)", api.Version),
		Long:  fmt.Sprintf("XHDMI tools (version %s)\nAPI tools for XHDMI based modkits, e.g. PS1Digital", api.Version),
	}

	versionCmd = &cobra.Command{
		Use:   "version",
		Short: fmt.Sprintf("Print %s version", path.Base(os.Args[0])),
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("%s (%s, build time: %s)\n", api.Version, api.Commit, api.Date)
		},
	}
)

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&consoleHostname, "hostname", "", "hostname")
	rootCmd.PersistentFlags().StringVar(&consoleUser, "user", "", "user")
	rootCmd.PersistentFlags().StringVar(&consolePassword, "password", "", "password")
	rootCmd.AddCommand(versionCmd)
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func initConfig() {
	home, err := homedir.Dir()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	defaultConfigPath := fmt.Sprintf("%s/.config/xhdmi-tools", home)

	viper.SetConfigType("yaml")
	viper.SetConfigPermissions(os.FileMode(0600))
	viper.SetConfigName("config")
	viper.AddConfigPath(defaultConfigPath)

	viper.AutomaticEnv()

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err != nil {
		defaultConfig := fmt.Sprintf("%s/%s", defaultConfigPath, "config.yaml")
		if _, err := os.Stat(defaultConfig); os.IsNotExist(err) {
			//fmt.Printf("Creating config file: [%s]\n", defaultConfig)
			os.MkdirAll(defaultConfigPath, os.FileMode(0700))
			viper.WriteConfigAs(defaultConfig)
		} else {
			//fmt.Printf("1: Read config file: [%s]\n", viper.ConfigFileUsed())
		}
	} else {
		//fmt.Printf("2: Read config file: [%s]\n", viper.ConfigFileUsed())
	}
}

func doYAMLOutput(i interface{}) error {
	w := bufio.NewWriter(os.Stdout)
	yamlEnc := yaml.NewEncoder(w)
	yamlEnc.SetIndent(2)
	err := yamlEnc.Encode(i)
	yamlEnc.Close()
	w.Flush()
	return err
}
