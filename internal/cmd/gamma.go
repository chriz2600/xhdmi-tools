package cmd

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"gitlab.com/xhdmi-tools/pkg/api"
)

// gammaCmd represents the gamma command
var (
	reservedGammaMapNames = []string{"-0.4", "-0.3", "-0.2", "-0.1", "off", "+0.1", "+0.2", "+0.3", "+0.4"}

	gammaCmd = &cobra.Command{
		Use:   "gamma",
		Short: "Handles XHDMI gamma maps",
	}

	gammaCreateCSVCmd = &cobra.Command{
		Use:   "createcsv VALUE [OUTPUT]",
		Short: "Creates gamma map csv for specified gamma value (offline)",
		Args:  cobra.RangeArgs(1, 2),
		RunE: func(cmd *cobra.Command, args []string) error {
			var (
				output string
				err    error
			)

			if len(args) > 1 {
				output = strings.TrimSpace(args[1])
			}

			if args[0] == "rgb555" {
				err = api.ColorconvCSV(output, 5, 5, 5)
			} else if args[0] == "rgb565" {
				err = api.ColorconvCSV(output, 5, 6, 5)
			} else {
				gamma, err := strconv.ParseFloat(args[0], 64)
				if err != nil {
					return err
				}
				err = api.GammaMapCSV(output, gamma)
			}

			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully created gamma csv for %s to %s\n", args[0], output)
			}
			return err
		},
	}

	gammaToCSVCmd = &cobra.Command{
		Use:   "tocsv INPUTDATA CSVOUTPUT",
		Short: "Creates a gamma map csv file from a gamma map data file (offline)",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			input := strings.TrimSpace(args[0])
			output := strings.TrimSpace(args[1])
			err := api.CreateCSVFromGammaData(input, output)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully created %s from %s\n", output, input)
			}
			return err
		},
	}

	gammaCreateDataCmd = &cobra.Command{
		Use:   "fromcsv CSVFILE OUTPUT",
		Short: "Creates gamma map data file from gamma map csv file (offline)",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			input := strings.TrimSpace(args[0])
			output := strings.TrimSpace(args[1])

			err := api.GammaCSVToData(input, output)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully created gamma data %s from %s\n", output, input)
			}
			return err
		},
	}

	gammaDownloadCmd = &cobra.Command{
		Use:   "download NAME FILE",
		Short: "Downloads a gamma map data file from the console",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			name := strings.TrimSpace(args[0])
			filename := strings.TrimSpace(args[1])
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			err = console.DownloadGammaMap(name, filename)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully downloaded %s to %s\n", name, filename)
			}
			return err
		},
	}

	gammaUploadCmd = &cobra.Command{
		Use:   "upload FILE NAME",
		Short: "Uploads a gamma map data file to the console",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			filename := strings.TrimSpace(args[0])
			name := strings.TrimSpace(args[1])
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			if isReservedGammaMap(name) {
				return fmt.Errorf("%s is a reserved gamma map", name)
			}
			err = console.UploadGammaMap(filename, name)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully uploaded %s as %s\n", filename, name)
			}
			return err
		},
	}

	gammaListCmd = &cobra.Command{
		Use:   "list",
		Short: "List gamma maps installed on console",
		RunE: func(cmd *cobra.Command, args []string) error {
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			fl, err := console.ListGammaMaps()
			if err != nil {
				return err
			}

			w := tabwriter.NewWriter(os.Stdout, 0, 0, 3, ' ', 0)
			fmt.Fprintln(w, "ACTIVE\tNAME\t")
			for _, s := range fl.Gammas {
				var marker string
				if s.Active {
					marker = "*"
				} else {
					marker = ""
				}
				fmt.Fprintf(w, "%s\t%s\t\n", marker, s.Name)
			}
			w.Flush()

			return err
		},
	}

	gammaDeleteCmd = &cobra.Command{
		Use:   "delete NAME",
		Short: "Deletes a gamma map from the console",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			name := strings.TrimSpace(args[0])
			if name == "default" {
				return errors.New("Font 'default' cannot be deleted")
			}
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			if isReservedGammaMap(name) {
				return fmt.Errorf("%s is a reserved gamma map", name)
			}
			err = console.DeleteGammaMap(name)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully deleted font %s\n", name)
			}
			return err
		},
	}

	gammaLoadCmd = &cobra.Command{
		Use:   "show NAME",
		Short: "Shows a gamma map on the console",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			name := strings.TrimSpace(args[0])
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			err = console.LoadGammaMap(name)
			return err
		},
	}
)

func init() {
	cobra.EnableCommandSorting = false
	rootCmd.AddCommand(gammaCmd)
	gammaCmd.AddCommand(gammaListCmd)

	gammaCmd.AddCommand(gammaDownloadCmd)
	gammaDownloadCmd.MarkZshCompPositionalArgumentWords(1, "NAME")
	gammaDownloadCmd.MarkZshCompPositionalArgumentFile(2)

	gammaCmd.AddCommand(gammaUploadCmd)
	gammaUploadCmd.MarkZshCompPositionalArgumentFile(1)
	gammaUploadCmd.MarkZshCompPositionalArgumentWords(2, "NAME")

	gammaCmd.AddCommand(gammaDeleteCmd)
	gammaDeleteCmd.MarkZshCompPositionalArgumentWords(1, "NAME")

	gammaCmd.AddCommand(gammaLoadCmd)
	gammaLoadCmd.MarkZshCompPositionalArgumentWords(1, "NAME")

	gammaCmd.AddCommand(gammaCreateCSVCmd)
	gammaCreateCSVCmd.MarkZshCompPositionalArgumentWords(1, "VALUE")
	gammaCreateCSVCmd.MarkZshCompPositionalArgumentFile(2)

	gammaCmd.AddCommand(gammaToCSVCmd)
	gammaToCSVCmd.MarkZshCompPositionalArgumentFile(1)
	gammaToCSVCmd.MarkZshCompPositionalArgumentFile(2)

	gammaCmd.AddCommand(gammaCreateDataCmd)
	gammaCreateDataCmd.MarkZshCompPositionalArgumentFile(1)
	gammaCreateDataCmd.MarkZshCompPositionalArgumentFile(2)
}

func isReservedGammaMap(name string) bool {
	for _, n := range reservedGammaMapNames {
		if name == n {
			return true
		}
	}
	return false
}
