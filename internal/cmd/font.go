package cmd

import (
	"bufio"
	"errors"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"os"
	"strconv"
	"strings"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"gitlab.com/xhdmi-tools/pkg/api"
)

var (
	fontRootCmd = &cobra.Command{
		Use:   "font",
		Short: "Handles XHDMI font definition files",
	}

	fonGetDefaultCmd = &cobra.Command{
		Use:   "getdefault FILE",
		Short: "Saves the default font png to FILE (offline)",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			var (
				writer *os.File
				err    error
			)
			filename := strings.TrimSpace(args[0])
			writer = os.Stdout
			if filename != "" && filename != "-" {
				writer, err = os.OpenFile(filename, os.O_CREATE|os.O_RDWR|os.O_TRUNC, os.FileMode(0644))
				if err != nil {
					return err
				}
				defer writer.Close()
			}
			_, err = writer.Write(defaultFontPNG)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully saved default font png to %s\n", filename)
			}
			return err
		},
	}

	fontFromImageCmd = &cobra.Command{
		Use:   "fromimage IMAGE OUTPUT",
		Short: "Creates a font data file from a png (offline)",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			input := strings.TrimSpace(args[0])
			output := strings.TrimSpace(args[1])
			isV2, err := cmd.Flags().GetBool("v2")

			if err != nil {
				return err
			}

			err = api.CreateFontData(input, output, isV2)
			if err == nil {
				versionStr := "v1"
				if isV2 {
					versionStr = "v2"
				}
				fmt.Fprintf(os.Stderr, "Successfully created %s from %s (%s)\n", output, input, versionStr)
			}
			return err
		},
	}

	fontToImageCmd = &cobra.Command{
		Use:   "toimage INPUT IMAGE",
		Short: "Creates a png from a font data file (offline)",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			input := strings.TrimSpace(args[0])
			output := strings.TrimSpace(args[1])
			isV2, err := cmd.Flags().GetBool("v2")

			err = api.CreateImageFromFontData(input, output, isV2)
			if err == nil {
				versionStr := "v1"
				if isV2 {
					versionStr = "v2"
				}
				fmt.Fprintf(os.Stderr, "Successfully created %s from %s (%s)\n", output, input, versionStr)
			}
			return err
		},
	}

	fontDownloadCmd = &cobra.Command{
		Use:   "download NAME FILE",
		Short: "Downloads a font data file from the console",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			name := strings.TrimSpace(args[0])
			filename := strings.TrimSpace(args[1])
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			err = console.DownloadFont(name, filename)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully downloaded %s to %s\n", name, filename)
			}
			return err
		},
	}

	fontUploadCmd = &cobra.Command{
		Use:   "upload FILE NAME",
		Short: "Uploads a font data file to the console",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			filename := strings.TrimSpace(args[0])
			name := strings.TrimSpace(args[1])
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			err = console.UploadFont(filename, name)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully uploaded %s as %s\n", filename, name)
			}
			return err
		},
	}

	fontListCmd = &cobra.Command{
		Use:   "list",
		Short: "List fonts installed on console",
		RunE: func(cmd *cobra.Command, args []string) error {
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			fl, err := console.ListFonts()
			if err != nil {
				return err
			}

			w := tabwriter.NewWriter(os.Stdout, 0, 0, 3, ' ', 0)
			fmt.Fprintln(w, "ACTIVE\tNAME\t")
			for _, s := range fl.Fonts {
				var marker string
				if s.Active {
					marker = "*"
				} else {
					marker = ""
				}
				fmt.Fprintf(w, "%s\t%s\t\n", marker, s.Name)
			}
			w.Flush()

			return err
		},
	}

	fontDeleteCmd = &cobra.Command{
		Use:   "delete NAME",
		Short: "Deletes a font from the console",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			name := strings.TrimSpace(args[0])
			if name == "default" {
				return errors.New("Font 'default' cannot be deleted")
			}
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			err = console.DeleteFont(name)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully deleted font %s\n", name)
			}
			return err
		},
	}

	fontLoadCmd = &cobra.Command{
		Use:   "show NAME",
		Short: "Shows a font on the console (this also activates the osd)",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			name := strings.TrimSpace(args[0])
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			err = console.LoadFont(name)
			return err
		},
	}

	fontRemoveSpacingCmd = &cobra.Command{
		Use:   "remspacing INPUT OUTPUT",
		Short: "Copies INPUT image to OUTPUT image removing spacing around characters (offline)",
		Args:  cobra.ExactArgs(3),
		RunE: func(cmd *cobra.Command, args []string) error {
			input := strings.TrimSpace(args[0])
			output := strings.TrimSpace(args[1])
			spacing, err := strconv.ParseInt(args[2], 10, 32)
			err = ConvertImage(input, output, int(spacing))
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully created %s from %s\n", output, input)
			}
			return err
		},
	}
)

func init() {
	cobra.EnableCommandSorting = false
	rootCmd.AddCommand(fontRootCmd)
	fontRootCmd.AddCommand(fontListCmd)

	fontRootCmd.AddCommand(fontDownloadCmd)
	fontDownloadCmd.MarkZshCompPositionalArgumentWords(1, "NAME")
	fontDownloadCmd.MarkZshCompPositionalArgumentFile(2)

	fontRootCmd.AddCommand(fontUploadCmd)
	fontUploadCmd.MarkZshCompPositionalArgumentFile(1)
	fontUploadCmd.MarkZshCompPositionalArgumentWords(2, "NAME")

	fontRootCmd.AddCommand(fontDeleteCmd)
	fontDeleteCmd.MarkZshCompPositionalArgumentWords(1, "NAME")

	fontRootCmd.AddCommand(fontLoadCmd)
	fontLoadCmd.MarkZshCompPositionalArgumentWords(1, "NAME")

	fontRootCmd.AddCommand(fonGetDefaultCmd)
	fonGetDefaultCmd.MarkZshCompPositionalArgumentFile(1)

	fontRootCmd.AddCommand(fontToImageCmd)
	fontToImageCmd.MarkZshCompPositionalArgumentFile(1)
	fontToImageCmd.MarkZshCompPositionalArgumentFile(2)
	fontToImageCmd.Flags().Bool("v2", false, "use Pixel FX v2 format")

	fontRootCmd.AddCommand(fontFromImageCmd)
	fontFromImageCmd.MarkZshCompPositionalArgumentFile(1)
	fontFromImageCmd.MarkZshCompPositionalArgumentFile(2)
	fontFromImageCmd.Flags().Bool("v2", false, "use Pixel FX v2 format")

	//fontRootCmd.AddCommand(fontRemoveSpacingCmd)

}

func ConvertImage(inputName, outputName string, spacing int) error {
	const (
		maxAddr    = 3072
		charWidth  = 8
		charHeight = 16
	)

	var (
		err    error
		reader *os.File
		writer *os.File
	)

	reader = os.Stdin
	if inputName != "" && inputName != "-" {
		reader, err = os.Open(inputName)
		if err != nil {
			return err
		}
		defer reader.Close()
	}

	writer = os.Stdout
	if outputName != "" && outputName != "-" {
		writer, err = os.OpenFile(outputName, os.O_RDWR|os.O_CREATE|os.O_TRUNC, os.FileMode(0644))
		if err != nil {
			return err
		}
		defer writer.Close()
	}

	imgIn, err := png.Decode(bufio.NewReader(reader))
	if err != nil {
		return err
	}
	// inputWidth := imgIn.Bounds().Max.X
	// inputHeight := imgIn.Bounds().Max.Y

	columns := 16
	rowHeight := 16
	rows := 11
	iwidth := columns * 8
	iheight := rows * rowHeight

	r := image.Rectangle{}
	r.Min.X = 0
	r.Min.Y = 0
	r.Max.X = iwidth
	r.Max.Y = iheight

	imgOut := image.NewGray(r)

	// xout := 0
	// yout := 0
	// for y := 0; y < inputWidth; y++ {
	// 	for x := 0; x < inputHeight; x++ {

	// 		c := color.GrayModel.Convert(imgIn.At(x, y)).(color.Gray)

	// 		imgOut.Set(x, y, c)
	// 	}
	// }
	for y := 0; y < iheight; y++ {
		for x := 0; x < iwidth; x++ {
			c := color.GrayModel.Convert(imgIn.At(x+int(x/charWidth*spacing), y+int(y/charHeight*spacing))).(color.Gray)
			imgOut.Set(x, y, c)
		}
	}

	png.Encode(writer, imgOut)
	return nil
}
