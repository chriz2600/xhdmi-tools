package cmd

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"reflect"
	"strings"
	"sync"
	"syscall"
	"text/tabwriter"
	"time"

	"github.com/grandcat/zeroconf"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/xhdmi-tools/pkg/api"
	"golang.org/x/crypto/ssh/terminal"
	"gopkg.in/yaml.v3"
)

type ConsoleProcess struct {
	URI  string
	Name string
}

// consoleCmd represents the console command
var (
	FPGADownload   = ConsoleProcess{URI: "/download/fpga", Name: "FPGA"}
	ESPDownload    = ConsoleProcess{URI: "/download/esp", Name: "ESP"}
	IndexDownload  = ConsoleProcess{URI: "/download/index", Name: "Index"}
	GamedbDownload = ConsoleProcess{URI: "/download/gamedb", Name: "GameDB"}
	FPGAFlash      = ConsoleProcess{URI: "/flash/fpga", Name: "FPGA"}
	ESPFlash       = ConsoleProcess{URI: "/flash/esp", Name: "ESP"}
	IndexFlash     = ConsoleProcess{URI: "/flash/index", Name: "Index"}

	IAPMode = "iap-mode"

	testdataMonitor   bool
	testdataPinLabels = []string{"R0", "R1", "R2", "R3", "R4", "R5", "R6", "R7", "G0", "G1", "G2", "G3", "G4", "G5", "G6", "G7", "B0", "B1", "B2", "B3", "B4", "B5", "B6", "B7"}

	demoSysinfo     *api.Sysinfo = &api.Sysinfo{}
	pollInterval                 = 250 * time.Millisecond
	demoSysinfoYaml              = `isdemodata: false
sdkversion: v3.3.1
cpufreqmhz: 240
i2cerrorcount: 0
fs:
    total: 10031104
    free: 7557120
    used: 2473984
memory:
    sketch:
        total: 3276800
        free: 2359376
        used: 917424
    heap:
        total: 532480
        free: 177684
        min: 125276
wifi:
    mac:
        station: 00:11:22:33:44:55
        accesspoint: 00:11:22:33:44:56
    network:
        address: 192.168.178.178
        gateway: 192.168.178.1
        netmask: 255.255.255.0
        dnsserver: 192.168.178.1
    rssi: -56
    quality: 88
    channel: 1
chip:
    type: esp32
    cores: 2
    revision: 1
    features: 50
currentvideo:
    resolutiondata: 68
    resolution: 4
    captureadjust: 5
    deinterlacermode: 0
    osdresolution: 640x480i->1080p
    colorexpansion: 7
    gammamode: 31
firmware:
    model: PS1Digital
    version: v1.0.0
testdata: '00 00 00 00 00 00 00 00 00 00 00 02 00 00 00 01 7c 7c 7c 78 f8 f8 1a 91 06 00 00 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 '`

	consoleCmd = &cobra.Command{
		Use:   "console",
		Short: "Manage consoles",
		Long:  `A console is represented by a user defined NAME and a HOSTNAME/USER/PASSWORD combination.`,
	}

	// createCmd represents the create command
	consoleCreateCmd = &cobra.Command{
		Use:   "add NAME",
		Short: "Adds a console",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			name := strings.TrimSpace(args[0])

			var (
				newConsole api.Console
				err        error
				password   string
			)

			if name == "" {
				return errors.New("invalid name")
			}

			if name == IAPMode {
				return fmt.Errorf("%s is a reserved name", IAPMode)
			}

			_, ctx := getConsole(name)
			if ctx != nil {
				return errors.New("console already exists")
			}

			newConsole.Name = name
			newConsole.Hostname, err = cmd.Flags().GetString("hostname")
			if err != nil {
				return err
			}
			newConsole.User, err = cmd.Flags().GetString("user")
			if err != nil {
				return err
			}

			password = consolePassword
			if password != "" {
				newConsole.Password = password
			} else {
				for {
					if terminal.IsTerminal(int(syscall.Stdin)) {
						fmt.Printf("Password: ")
						_password, err := terminal.ReadPassword(int(syscall.Stdin))
						if err != nil {
							return err
						}
						fmt.Printf("\n")
						password = string(bytes.TrimSpace(_password))
					} else {
						reader := bufio.NewReader(os.Stdin)
						password, err = reader.ReadString('\n')
						password = strings.TrimSpace(password)
					}
					if password == "" {
						continue
					}
					newConsole.Password = password
					break
				}
			}

			ctxs := getConsoles()
			ctxs = append(ctxs, newConsole)

			viper.Set("consoles", ctxs)
			_ctx, _ := getCurrentConsole()
			if _ctx == nil {
				viper.Set("current_console", name)
			}
			err = viper.WriteConfig()
			if err != nil {
				return err
			}

			return nil
		},
	}

	// deleteCmd represents the delete command
	consoleDeleteCmd = &cobra.Command{
		Use:   "remove NAME",
		Short: "Removes a console",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			name := strings.TrimSpace(args[0])

			if name == "" {
				return errors.New("invalid name")
			}

			if name == IAPMode {
				return fmt.Errorf("%s is a reserved name", IAPMode)
			}

			pos, ctx := getConsole(name)
			if ctx == nil {
				return errors.New("console not found")
			}

			ctxs := getConsoles()
			ctxs = append(ctxs[:pos], ctxs[pos+1:]...)

			viper.Set("consoles", ctxs)
			viper.WriteConfig()

			return nil
		},
	}

	// deleteCmd represents the delete command
	consoleListCmd = &cobra.Command{
		Use:   "list",
		Short: "List consoles",
		RunE: func(cmd *cobra.Command, args []string) error {
			currentConsole := viper.GetString("current_console")
			ctxs := getConsoles()

			w := tabwriter.NewWriter(os.Stdout, 0, 0, 3, ' ', 0)
			fmt.Fprintln(w, "ACTIVE\tNAME\tHOSTNAME\tUSER\t")
			for _, s := range ctxs {
				var marker string
				if s.Name == currentConsole {
					marker = "*"
				} else {
					marker = ""
				}
				fmt.Fprintf(w, "%s\t%s\t%s\t%s\t\n", marker, s.Name, s.Hostname, s.User)
			}
			w.Flush()

			return nil
		},
	}

	consoleUseCmd = &cobra.Command{
		Use:   "use NAME",
		Short: "Set the active console",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			name := strings.TrimSpace(args[0])

			if name == "" {
				return errors.New("invalid name")
			}

			_, ctx := getConsole(name)
			if ctx == nil {
				return errors.New("console not found")
			}

			viper.Set("current_console", name)
			viper.WriteConfig()

			return nil
		},
	}

	consoleResetCmd = &cobra.Command{
		Use:   "reset",
		Short: "Reset the active console",
		Args:  cobra.MaximumNArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			rtype := "all"
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			if len(args) > 0 {
				rtype = strings.TrimSpace(args[0])
			}
			err = console.Reset(rtype)
			if err != nil {
				return err
			}
			fmt.Printf("Reset request sent!\n")
			return nil
		},
	}

	consoleCleanupCmd = &cobra.Command{
		Use:   "cleanup",
		Short: "Removes all previously staged firmware",
		RunE: func(cmd *cobra.Command, args []string) error {
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			err = console.Cleanup()
			if err == nil {
				fmt.Printf("Cleanup OK!\n")
			}
			return err
		},
	}

	consoleFwCmd = &cobra.Command{
		Use:   "fw",
		Short: "Console firmware functions",
	}

	consoleFwVersionCmd = &cobra.Command{
		Use:   "version",
		Short: "Get the console's firmware version",
		RunE: func(cmd *cobra.Command, args []string) error {
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}

			out := make(chan interface{})
			go console.RequestAsync(out, reflect.TypeOf(api.ActiveConsoleConfig{}), "/config", nil)

			for res := range out {
				result, ok := res.(*api.ActiveConsoleConfig)
				if !ok {
					err, ok := res.(error)
					if !ok {
						return fmt.Errorf("received unexpected type: %s", reflect.TypeOf(res))
					}
					return err
				}

				fmt.Printf("Installed firmware: %s %s\n", result.Model, result.Version)
				fmt.Printf("Firmware server:    %s\n", result.FirmwareServer)
				fmt.Printf("Firmware path:      %s\n", result.FirmwarePath)
				fmt.Printf("Firmware version:   %s\n", result.FirmwareVersion)
				break
			}
			return nil
		},
	}

	consoleFwAutoCmd = &cobra.Command{
		Use:   "auto",
		Short: "Automatically download/flash firmware",
		RunE: func(cmd *cobra.Command, args []string) error {
			var (
				console *api.Console
				result  *api.FWCheck
				err     error
			)

			console, err = getCurrentConsole()
			if err != nil {
				return err
			}

			resetNeeded := false
			loopcounter := 0
			for {
				if loopcounter >= 2 {
					return fmt.Errorf("Could not download files")
				}
				if loopcounter > 0 {
					fmt.Printf("Recheck for download!\n")
				} else {
					fmt.Printf("Check for download!\n")
				}
				result, err = console.CheckForFirmware()
				if err != nil {
					return err
				}

				parts := make([]ConsoleProcess, 0)
				if result.FPGA.Action == api.FWDownload {
					parts = append(parts, FPGADownload)
				}
				if result.ESP.Action == api.FWDownload {
					parts = append(parts, ESPDownload)
				}
				if result.Index.Action == api.FWDownload {
					parts = append(parts, IndexDownload)
				}
				if result.HasGameDB && result.Gamedb.Action == api.FWDownload {
					parts = append(parts, GamedbDownload)
				}

				if len(parts) == 0 {
					fmt.Printf("No download neccessary!\n")
					break
				}

				fmt.Printf("Downloading firmware!\n")
				err = execParts(console, parts)
				if err != nil {
					return err
				}
				loopcounter = loopcounter + 1
			}

			loopcounter = 0
			for {
				if loopcounter >= 2 {
					return fmt.Errorf("Could not flash files")
				}
				if loopcounter > 0 {
					fmt.Printf("Recheck for flash!\n")
				} else {
					fmt.Printf("Check for flash!\n")
				}
				result, err = console.CheckForFirmware()
				if err != nil {
					return err
				}

				parts := make([]ConsoleProcess, 0)
				if result.FPGA.Action == api.FWFlash {
					parts = append(parts, FPGAFlash)
					resetNeeded = true
				}
				if result.ESP.Action == api.FWFlash {
					parts = append(parts, ESPFlash)
					resetNeeded = true
				}
				if result.Index.Action == api.FWFlash {
					parts = append(parts, IndexFlash)
					resetNeeded = true
				}

				if len(parts) == 0 {
					fmt.Printf("No flash neccessary!\n")
					break
				}

				fmt.Printf("Flashing firmware!\n")
				err = execParts(console, parts)
				if err != nil {
					return err
				}
				loopcounter = loopcounter + 1
			}

			printCheckResult(result)

			if resetNeeded {
				fmt.Printf("Resetting console!\n")
				console.Reset("all")
			} else {
				fmt.Printf("No reset needed!\n")
			}
			fmt.Printf("Done!\n")

			// fmt.Printf("Recheck for needed steps!\n")
			// result, err = console.CheckForFirmware()
			// if err != nil {
			// 	return err
			// }

			return nil
		},
	}

	consoleFwChecksumCmd = &cobra.Command{
		Use:   "check",
		Short: "Check for new firmware version",
		RunE: func(cmd *cobra.Command, args []string) error {
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}

			result, err := console.CheckForFirmware()
			if err != nil {
				return err
			}

			printCheckResult(result)

			return nil
		},
	}

	consoleFwDownloadCmd = &cobra.Command{
		Use:   "download [TYPE]",
		Short: "Download firmware files to console",
		RunE: func(cmd *cobra.Command, args []string) error {
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}

			hasGameDB := console.HasGameDB()

			parts := []ConsoleProcess{FPGADownload, ESPDownload, IndexDownload}
			if hasGameDB {
				parts = append(parts, GamedbDownload)
			}

			if len(args) > 0 {
				fwType := strings.TrimSpace(args[0])
				if fwType != "" {
					switch fwType {
					case "fpga":
						parts = []ConsoleProcess{FPGADownload}
						break
					case "esp":
						parts = []ConsoleProcess{ESPDownload}
						break
					case "index":
						parts = []ConsoleProcess{IndexDownload}
						break
					case "gamedb":
						parts = []ConsoleProcess{GamedbDownload}
						break
					default:
						return fmt.Errorf("Only fpga, esp, index or gamedb are allowed TYPEs")
					}
				}
			}

			fmt.Printf("Downloading firmware!\n")
			err = execParts(console, parts)
			if err != nil {
				return err
			}
			fmt.Printf("Done!\n")
			return nil
		},
	}

	consoleFwFlashCmd = &cobra.Command{
		Use:   "flash [TYPE]",
		Short: "Flash firmware",
		RunE: func(cmd *cobra.Command, args []string) error {
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}

			parts := []ConsoleProcess{FPGAFlash, ESPFlash, IndexFlash}

			if len(args) > 0 {
				fwType := strings.TrimSpace(args[0])
				if fwType != "" {
					switch fwType {
					case "fpga":
						parts = []ConsoleProcess{FPGAFlash}
						break
					case "esp":
						parts = []ConsoleProcess{ESPFlash}
						break
					case "index":
						parts = []ConsoleProcess{IndexFlash}
						break
					default:
						return fmt.Errorf("Only fpga, esp or index are allowed TYPEs")
					}
				}
			}

			fmt.Printf("Flashing firmware!\n")
			err = execParts(console, parts)
			if err != nil {
				return err
			}
			fmt.Printf("Done!\n")
			return nil
		},
	}

	consoleFwUploadCmd = &cobra.Command{
		Use:   "upload FILE TYPE",
		Short: "Upload firmware file to console",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			filename := strings.TrimSpace(args[0])
			fwType := strings.TrimSpace(args[1])
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}

			hasGameDB := console.HasGameDB()

			if fwType != "fpga" && fwType != "esp" && fwType != "index" {
				if !hasGameDB {
					return fmt.Errorf("Only fpga, esp or index are allowed TYPEs")
				} else if fwType != "gamedb" {
					return fmt.Errorf("Only fpga, esp, index or gamedb are allowed TYPEs")
				}
			}

			now := time.Now()
			err = console.Upload(filename, fmt.Sprintf("/upload/%s", fwType), nil)

			if err == nil {
				fmt.Printf("Upload %s firmware from %s success!\n", fwType, filename)
			}
			fmt.Printf("took: %ds\n", time.Now().Sub(now)/1000/1000/1000)

			return err
		},
	}

	consoleSysinfoCmd = &cobra.Command{
		Use:   "sysinfo",
		Short: "Get sysinfo from console (ALPHA!)",
		RunE: func(cmd *cobra.Command, args []string) error {
			var result *api.Sysinfo

			console, err := getCurrentConsoleWithPing(false)
			if err != nil {
				return err
			}

			if console.Ping() {
				result, err = console.GetSysinfo(!testdataMonitor)
				if err != nil {
					return err
				}
			} else {
				if !testdataMonitor {
					return fmt.Errorf("console '%s' is not reachable", console.Name)
				}
				initDemodata()
				result = demoSysinfo
			}

			s := result.CurrentVideo.OSDResolution
			s = s[0 : strings.LastIndex(s, "p")+1]
			s = strings.ReplaceAll(s, " ", "->")
			result.CurrentVideo.OSDResolution = s
			yamlEnc := yaml.NewEncoder(os.Stdout)
			yamlEnc.SetIndent(2)
			err = yamlEnc.Encode(result)
			defer yamlEnc.Close()
			if err != nil {
				return err
			}
			return nil
		},
	}

	consolePingCmd = &cobra.Command{
		Use:   "ping",
		Short: "Ping console",
		RunE: func(cmd *cobra.Command, args []string) error {
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			fmt.Printf("console %s is alive!\n", console.Name)
			return nil
		},
	}

	// consoleShellCmd = &cobra.Command{
	// 	Use:   "shell",
	// 	Short: "Open xhdmi shell",
	// 	RunE: func(cmd *cobra.Command, args []string) error {
	// 		console, err := getCurrentConsole()
	// 		if err != nil {
	// 			return err
	// 		}
	// 		fmt.Printf("console %s is alive!\n", console.Name)

	// 		err = console.StartConsoleServer()
	// 		if err != nil {
	// 			return err
	// 		}

	// 		csState, err := console.GetConsoleServerState()
	// 		if err != nil {
	// 			return err
	// 		}
	// 		fmt.Printf("console server state: %s!\n", csState)

	// 		servAddr := console.Hostname + ":7713"
	// 		tcpAddr, err := net.ResolveTCPAddr("tcp", servAddr)
	// 		if err != nil {
	// 			return fmt.Errorf("resolve failed: %v", err)
	// 		}

	// 		conn, err := net.DialTCP("tcp", nil, tcpAddr)
	// 		if err != nil {
	// 			return fmt.Errorf("dial failed: %v", err)
	// 		}
	// 		defer conn.Close()

	// 		conn.SetNoDelay(true)

	// 		err = termbox.Init()
	// 		if err != nil {
	// 			return fmt.Errorf("termbox init failed: %v", err)
	// 		}
	// 		defer termbox.Close()
	// 		termbox.SetInputMode(termbox.InputAlt | termbox.InputMouse)
	// 		termbox.SetCursor(10, 10)

	// 		go func() {
	// 			buffer := make([]byte, 128)
	// 			for {
	// 				r, err := conn.Read(buffer)
	// 				if err == io.EOF {
	// 					break
	// 				}
	// 				if err != nil {
	// 					fmt.Printf("read failed: %v", err)
	// 					break
	// 				}
	// 				_, err = os.Stdout.Write(buffer[:r])
	// 				if err != nil {
	// 					fmt.Printf("write failed: %v", err)
	// 					break
	// 				}
	// 			}
	// 		}()

	// 	mainloop:
	// 		for {
	// 			// if cap(data)-len(data) < 32 {
	// 			// 	newdata := make([]byte, len(data), len(data)+32)
	// 			// 	copy(newdata, data)
	// 			// 	data = newdata
	// 			// }
	// 			// beg := len(data)
	// 			data := make([]byte, 64)
	// 			switch ev := termbox.PollRawEvent(data); ev.Type {
	// 			case termbox.EventRaw:
	// 				ev2 := termbox.ParseEvent(data[:ev.N])
	// 				current := fmt.Sprintf("%q", data[:ev.N])
	// 				//fmt.Printf("%s", current)
	// 				// CTRL+D to end terminal
	// 				if current == `"\x04"` {
	// 					break mainloop
	// 				}

	// 				if ev2.Type == termbox.EventMouse {

	// 				} else if current == `"\r"` {
	// 					conn.Write([]byte("\r\n"))
	// 				} else {
	// 					conn.Write(data[:ev.N])
	// 				}
	// 				// for {
	// 				// 	ev := termbox.ParseEvent(data)
	// 				// 	if ev.N == 0 {
	// 				// 		break
	// 				// 	}
	// 				// 	curev := ev
	// 				// 	copy(data, data[curev.N:])
	// 				// 	data = data[:len(data)-curev.N]
	// 				// }
	// 			case termbox.EventError:
	// 				return fmt.Errorf("termbox event error: %v", ev.Err)
	// 			}
	// 		}
	// 		return nil
	// 	},
	// }

	consoleTestdataCmd = &cobra.Command{
		Use:   "testdata",
		Short: "Get testdata from console (ALPHA!)",
		RunE: func(cmd *cobra.Command, args []string) error {
			if !testdataMonitor {
				console, err := getCurrentConsole()
				if err != nil {
					return err
				}

				buffer, err := console.GetTestdata()
				if err != nil {
					return err
				}
				data := getPinok(buffer)

				w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
				fmt.Fprintf(w, "Pin test:\n")
				fmt.Fprintf(w, "%s\n", strings.Join(testdataPinLabels, "\t"))

				for _, d := range data {
					s := "--"
					if d == 1 {
						s = "OK"
					}
					fmt.Fprintf(w, "%s\t", s)
				}
				fmt.Fprintf(w, "\n")

				resolX, resolY := getRawResolution(buffer)
				fmt.Fprintf(w, "Raw input resolution: %dx%d\n", resolX, resolY)
				w.Flush()
				return nil
			}

			return nil
		},
	}
)

func execParts(console *api.Console, parts []ConsoleProcess) error {
	var errCount int
	api.ShowProgressBegin()
	for _, p := range parts {
		_, err := console.GetRaw(p.URI, nil)
		if err != nil {
			return err
		}
		errCount, err = console.ShowProgress(p.Name)
		if err != nil {
			return fmt.Errorf("%d: %v", errCount, err)
		}
	}
	api.ShowProgressFinish()
	return nil
}

func getPinok(buffer []byte) []float64 {
	pinok1 := (uint32(buffer[16])<<16 | uint32(buffer[17])<<8 | uint32(buffer[18]))
	pinok2 := (uint32(buffer[19])<<16 | uint32(buffer[20])<<8 | uint32(buffer[21]))
	data := make([]float64, 24)
	for c := 0; c < 24; c++ {
		var p1 uint32 = uint32(c - 1)
		var p2 uint32 = uint32(c)
		if p1 > 22 {
			p1 = 0
		}
		if p2 > 22 {
			p2 = 22
		}
		data[c] = float64(isPinOK(pinok1, pinok2, p1, p2))
	}
	return data
}

func getRawResolution(buffer []byte) (uint16, uint16) {
	resolX := (uint16(buffer[22]) << 4) | (uint16(buffer[23]) >> 4)
	resolY := ((uint16(buffer[23]) & 0xF) << 8) | uint16(buffer[24])

	return resolX + 1, resolY + 1
}

type EventCounters struct {
	ADV struct {
		PllLol       uint32
		HotPlug      uint32
		MonitorSense uint32
	}
	Main struct {
		PllLol     uint32
		SiNotReady uint32
		Resync     uint32
	}
	PLL struct {
		ClkinLol uint32
		ALol     uint32
		BLol     uint32
	}
}

func getEventCounters(buffer []byte) EventCounters {
	ev := EventCounters{}
	ev.ADV.PllLol = (uint32(buffer[0])<<24 | uint32(buffer[1])<<16 | uint32(buffer[2])<<8 | uint32(buffer[3]))
	ev.ADV.HotPlug = (uint32(buffer[4])<<24 | uint32(buffer[5])<<16 | uint32(buffer[6])<<8 | uint32(buffer[7]))
	ev.ADV.MonitorSense = (uint32(buffer[35])<<24 | uint32(buffer[36])<<16 | uint32(buffer[37])<<8 | uint32(buffer[38]))
	ev.Main.PllLol = (uint32(buffer[8])<<24 | uint32(buffer[9])<<16 | uint32(buffer[10])<<8 | uint32(buffer[11]))
	ev.Main.SiNotReady = (uint32(buffer[12])<<24 | uint32(buffer[13])<<16 | uint32(buffer[14])<<8 | uint32(buffer[15]))
	ev.Main.Resync = (uint32(buffer[31])<<24 | uint32(buffer[32])<<16 | uint32(buffer[33])<<8 | uint32(buffer[34]))
	ev.PLL.ClkinLol = (uint32(buffer[39])<<24 | uint32(buffer[40])<<16 | uint32(buffer[41])<<8 | uint32(buffer[42]))
	ev.PLL.ALol = (uint32(buffer[43])<<24 | uint32(buffer[44])<<16 | uint32(buffer[45])<<8 | uint32(buffer[46]))
	ev.PLL.BLol = (uint32(buffer[47])<<24 | uint32(buffer[48])<<16 | uint32(buffer[49])<<8 | uint32(buffer[50]))
	return ev
}

type Command struct {
	URI    string
	Params map[string]string
}

func sendCommandChannel(ctx context.Context, wg *sync.WaitGroup, command <-chan *Command, console *api.Console) <-chan error {
	errc := make(chan error)
	wg.Add(1)
	go func() {
		var cmd *Command
		defer close(errc)
		defer wg.Done()
		for {
			select {
			case <-ctx.Done():
				return
			case cmd = <-command:
				if cmd != nil && console.Ping() {
					err := console.Invoke(cmd.URI, cmd.Params)
					cmd = nil
					if err != nil {
						errc <- err
						return
					}
				}
			}
		}
	}()
	return errc
}

func pollSysinfo(ctx context.Context, wg *sync.WaitGroup, console *api.Console) (<-chan *api.Sysinfo, <-chan error) {
	out := make(chan *api.Sysinfo, 1)
	errc := make(chan error)
	wg.Add(1)
	go func() {
		defer close(out)
		defer close(errc)
		defer wg.Done()
		var (
			data *api.Sysinfo
			err  error
		)
		for {
			if console.Ping() {
				data, err = console.GetSysinfo(false)
				if err != nil {
					errc <- err
					return
				}
			}
			select {
			case <-ctx.Done():
				return
			case out <- data:
				time.Sleep(pollInterval)
			}
		}
	}()
	return out, errc
}

func init() {
	cobra.EnableCommandSorting = false
	rootCmd.AddCommand(consoleCmd)
	consoleCmd.AddCommand(consoleCreateCmd)
	consoleCmd.AddCommand(consoleDeleteCmd)
	consoleCmd.AddCommand(consoleListCmd)
	consoleCmd.AddCommand(consoleUseCmd)
	consoleCmd.AddCommand(consoleResetCmd)
	consoleCmd.AddCommand(consoleCleanupCmd)
	consoleCmd.AddCommand(consoleFwCmd)
	consoleCmd.AddCommand(consoleTestdataCmd)
	consoleCmd.AddCommand(consoleSysinfoCmd)
	consoleCmd.AddCommand(consolePingCmd)
	//consoleCmd.AddCommand(consoleShellCmd)

	consoleCreateCmd.Flags().String("hostname", "", "Hostname (required)")
	consoleCreateCmd.Flags().String("user", "", "User (required)")
	consoleCreateCmd.MarkFlagRequired("user")
	consoleCreateCmd.MarkFlagRequired("hostname")
	consoleCreateCmd.MarkZshCompPositionalArgumentWords(1, "NAME")
	consoleDeleteCmd.MarkZshCompPositionalArgumentWords(1, "NAME")
	consoleUseCmd.MarkZshCompPositionalArgumentWords(1, "NAME")

	consoleFwCmd.AddCommand(consoleFwVersionCmd)
	consoleFwCmd.AddCommand(consoleFwChecksumCmd)
	consoleFwCmd.AddCommand(consoleFwDownloadCmd)
	consoleFwCmd.AddCommand(consoleFwFlashCmd)
	consoleFwCmd.AddCommand(consoleFwUploadCmd)
	consoleFwCmd.AddCommand(consoleFwAutoCmd)

	consoleFwUploadCmd.MarkZshCompPositionalArgumentFile(1)
	consoleFwUploadCmd.MarkZshCompPositionalArgumentWords(2, "fpga", "esp", "index")

	consoleTestdataCmd.Flags().BoolVar(&testdataMonitor, "monitor", false, "monitor data")
	consoleSysinfoCmd.Flags().BoolVar(&testdataMonitor, "monitor", false, "monitor data")
}

func initDemodata() {
	err := yaml.Unmarshal([]byte(demoSysinfoYaml), demoSysinfo)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
		return
	}
	demoSysinfo.IsDemoData = true
}

func getCurrentConsole() (*api.Console, error) {
	return getCurrentConsoleWithPing(true)
}

func resolveMDNSName(nameToLookFor string) (string, error) {
	// Discover all services on the network (e.g. _workstation._tcp)
	var ipAddress net.IP

	if !strings.HasSuffix(nameToLookFor, ".local") {
		return nameToLookFor, nil
	}

	resolver, err := zeroconf.NewResolver(nil)
	if err != nil {
		log.Fatalln("Failed to initialize resolver:", err.Error())
	}

	entries := make(chan *zeroconf.ServiceEntry)
	go func(results <-chan *zeroconf.ServiceEntry) {
		for entry := range results {
			if strings.HasPrefix(entry.HostName, nameToLookFor) {
				ipAddress = entry.AddrIPv4[0]
				break
			}
		}
	}(entries)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	err = resolver.Browse(ctx, "_http._tcp", "local.", entries)

	<-ctx.Done()
	if err != nil {
		return nameToLookFor, nil
	}

	if len(ipAddress) < 4 {
		return "", fmt.Errorf("Hostname '%s' could not be resolved", nameToLookFor)
	}

	return fmt.Sprintf("%d.%d.%d.%d", ipAddress[0], ipAddress[1], ipAddress[2], ipAddress[3]), nil
}

func getCurrentConsoleWithPing(doping bool) (*api.Console, error) {
	var err error
	_, console := getConsole(viper.GetString("current_console"))
	if console == nil {
		// create console on the fly from command line data
		if consoleHostname != "" && consoleUser != "" && consolePassword != "" {
			fmt.Fprintf(os.Stderr, "Using console from command line\n")
			console := api.Console{}
			console.Hostname, err = resolveMDNSName(consoleHostname)
			console.User = consoleUser
			console.Password = consolePassword
			if err != nil {
				return nil, err
			}
			if doping && !console.Ping() {
				return nil, fmt.Errorf("console '%s' is not reachable", console.Name)
			}
			return &console, nil
		}
		return nil, errors.New("No active console configuration found or name not resolve-able, is the console running?")
	}
	// overwrite stored data from command line
	if consoleHostname != "" {
		fmt.Fprintf(os.Stderr, "Using hostname from command line\n")
		console.Hostname, err = resolveMDNSName(consoleHostname)
		if err != nil {
			return nil, err
		}
	}
	if consoleUser != "" {
		fmt.Fprintf(os.Stderr, "Using user from command line\n")
		console.User = consoleUser
	}
	if consolePassword != "" {
		fmt.Fprintf(os.Stderr, "Using password from command line\n")
		console.Password = consolePassword
	}
	if doping && !console.Ping() {
		return nil, fmt.Errorf("console '%s' is not reachable", console.Name)
	}
	console.Hostname, err = resolveMDNSName(console.Hostname)
	return console, err
}

func getConsole(name string) (int, *api.Console) {
	var err error
	ctxs := getConsoles()
	for pos, s := range ctxs {
		if s.Name == name {
			s.Hostname, err = resolveMDNSName(s.Hostname)
			if err == nil {
				return pos, &s
			}
		}
	}
	return -1, nil
}

func getConsoles() []api.Console {
	var ctxs []api.Console
	var iapFound bool = false
	viper.UnmarshalKey("consoles", &ctxs)

	iapCtx := api.Console{}
	iapCtx.Name = IAPMode
	iapCtx.Hostname = "192.168.4.1"
	iapCtx.User = "please"
	iapCtx.Password = "installme!"

	for _, x := range ctxs {
		if x.Name == IAPMode {
			iapFound = true
		}
	}

	if !iapFound {
		ctxs = append(ctxs, iapCtx)
	}
	return ctxs
}

func isPinOK(pinok1, pinok2 uint32, pos1, pos2 uint32) int {
	if (pinok1&(1<<pos1)) != 0 && (pinok2&(1<<pos1)) != 0 && (pinok1&(1<<pos2)) != 0 && (pinok2&(1<<pos2)) != 0 {
		return 1
	}
	return 0
}

func printCheckResult(result *api.FWCheck) {
	w := tabwriter.NewWriter(os.Stdout, 0, 0, 3, ' ', 0)
	fmt.Fprintf(w, "Firmware Part\tChecksum Installed\tChecksum Staged\tChecksum Server\tResult\t\n")

	fResult := "Download!"
	eResult := "Download!"
	iResult := "Download!"
	gResult := "Download!"
	if result.FPGA.Action == api.FWOK {
		fResult = "OK"
	} else if result.FPGA.Action == api.FWFlash {
		fResult = "Flash!"
	}
	if result.ESP.Action == api.FWOK {
		eResult = "OK"
	} else if result.ESP.Action == api.FWFlash {
		eResult = "Flash!"
	}
	if result.Index.Action == api.FWOK {
		iResult = "OK"
	} else if result.Index.Action == api.FWFlash {
		iResult = "Flash!"
	}
	if result.Gamedb.Action == api.FWOK {
		gResult = "OK"
	}

	fmt.Fprintf(w, "FPGA\t%.16s\t%.16s\t%.16s\t%s\t\n", result.FPGA.Installed, result.FPGA.Staged, result.FPGA.Server, fResult)
	fmt.Fprintf(w, "ESP\t%.16s\t%.16s\t%.16s\t%s\t\n", result.ESP.Installed, result.ESP.Staged, result.ESP.Server, eResult)
	fmt.Fprintf(w, "Index\t%.16s\t%.16s\t%.16s\t%s\t\n", result.Index.Installed, result.Index.Staged, result.Index.Server, iResult)
	if result.HasGameDB {
		fmt.Fprintf(w, "GameDB\t%.16s\t%.16s\t%.16s\t%s\t\n", result.Gamedb.Installed, result.Gamedb.Staged, result.Gamedb.Server, gResult)
	}
	//fmt.Fprintf(w, "\nFirmware Version referenced: %s\t\n", result.FirmwareVersion)
	w.Flush()
}
