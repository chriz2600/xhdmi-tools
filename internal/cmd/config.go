package cmd

import (
	"bytes"
	"database/sql"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	meddler "github.com/russross/meddler"
	"github.com/spf13/cobra"
	"gitlab.com/xhdmi-tools/pkg/api"
	"gopkg.in/yaml.v3"
	_ "github.com/mattn/go-sqlite3"
	// _ "modernc.org/sqlite"
	// _ "github.com/glebarez/go-sqlite"
)

type GenericData struct {
	GameId  string `meddler:"gameId" yaml:"-"`
	Context string `meddler:"context" yaml:"context"`
	Key     string `meddler:"key" yaml:"key"`
	Value   []byte `meddler:"value" yaml:"value,flow"`
}

type GenericDatas struct {
	GameId string         `yaml:"gameid"`
	Data   []*GenericData `yaml:"data"`
}

type GameDbEntry struct {
	GameId  string
	Context string
	Key     string
	Value   []byte
}

var (
	configCmd = &cobra.Command{
		Use:   "config",
		Short: "Manage console config",
		Long:  `Download and upload console config file`,
	}

	confDbCmd = &cobra.Command{
		Use:   "db",
		Short: "Manage per game settings config db",
		Long:  `Download, upload and modify per game settings config db`,
	}

	configDownloadCmd = &cobra.Command{
		Use:   "download FILE",
		Short: "Get config from active console",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			filename := strings.TrimSpace(args[0])
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			err = console.DownloadConfig(filename, false)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully downloaded config to %s\n", filename)
			}
			return err
		},
	}

	configUploadCmd = &cobra.Command{
		Use:   "upload FILE",
		Short: "Upload config to active console",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			filename := strings.TrimSpace(args[0])
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			err = console.UploadConfig(filename)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully uploaded config from %s\n", filename)
			}
			return err
		},
	}

	configDbDownloadCmd = &cobra.Command{
		Use:   "download DBDIRECTORY",
		Short: "Get config database from active console (per game settings)",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			dirname := strings.TrimSpace(args[0])
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}

			fileInfo, err := os.Stat(dirname)
			if err == nil {
				if !fileInfo.IsDir() {
					return fmt.Errorf("%s is not a directoy", dirname)
				}
			}
			os.MkdirAll(dirname, os.FileMode(0755))

			filename := dirname + "/config.db"
			err = console.Download("/config.db", filename)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully downloaded config database to %s\n", filename)
			}
			filename = dirname + "/config.yaml"
			err = console.DownloadConfig(filename, true)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully downloaded global config to %s\n", filename)
			}
			err = checkAndMigrateGameDbFull(dirname)
			if err != nil {
				return err
			}

			return err
		},
	}

	configDbUploadCmd = &cobra.Command{
		Use:   "upload DBDIRECTORY",
		Short: "Upload (per game) config database to active console",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			dirname := strings.TrimSpace(args[0])
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}

			fileInfo, err := os.Stat(dirname)
			if err == nil {
				if !fileInfo.IsDir() {
					return fmt.Errorf("%s is not a directoy", dirname)
				}
			}

			err = checkAndMigrateGameDbFull(dirname)
			if err != nil {
				return err
			}

			filename := dirname + "/config.db"
			err = console.Upload(filename, "/upload/config.db", nil)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully uploaded config database from %s\n", filename)
			}

			return err
		},
	}

	configDbDeleteCmd = &cobra.Command{
		Use:   "delete",
		Short: "Delete (per game) config database on active console",
		RunE: func(cmd *cobra.Command, args []string) error {
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			err = console.Invoke("/delete/pgs", nil)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully deleted config database\n")
			}
			return err
		},
	}

	configDbListGameidsCmd = &cobra.Command{
		Use:   "list DBDIRECTORY [SEARCH]",
		Short: "List per game config database gameids",
		Args:  cobra.MinimumNArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			dirname := strings.TrimSpace(args[0])
			search := "%"

			if len(args) > 1 {
				search = strings.TrimSpace(args[1])
			}

			search = strings.ReplaceAll(search, "*", "%")

			err := checkAndMigrateGameDbFull(dirname)
			if err != nil {
				return err
			}

			filename := dirname + "/config.db"
			data, err := listGameIds(filename, search)
			if err != nil {
				return err
			}

			for _, d := range data {
				fmt.Fprintf(os.Stdout, "%s\n", d)
			}

			return nil
		},
	}

	configDbExportCmd = &cobra.Command{
		Use:   "export DBDIRECTORY EXPORTDIR [GAMEID]",
		Short: "Export per game config data for a given gameid or all games",
		Args:  cobra.MinimumNArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			var (
				err      error
				gids     []string
				fileInfo os.FileInfo
			)

			dbdir := strings.TrimSpace(args[0])
			exportdir := strings.TrimSpace(args[1])
			gameid := "*"

			if len(args) > 2 {
				gameid = strings.TrimSpace(args[2])
			}

			gids = []string{}

			if gameid == "*" {
				gids, err = listGameIds(dbdir+"/config.db", "%")
				if err != nil {
					return err
				}
			} else {
				gids = append(gids, gameid)
			}

			fileInfo, err = os.Stat(exportdir)
			if err == nil {
				if !fileInfo.IsDir() {
					return fmt.Errorf("%s is not a directoy", exportdir)
				}
			}
			os.MkdirAll(exportdir, os.FileMode(0755))

			err = checkAndMigrateGameDbFull(dbdir)
			if err != nil {
				return err
			}

			for _, gid := range gids {
				fname := exportdir + "/" + gid + ".pgsd"
				err = exportGameDbData(gid, dbdir, fname)
				if err != nil {
					return err
				}
				fmt.Fprintf(os.Stderr, "Successfully exported game db data for %s to %s\n", gid, fname)
			}

			return nil
		},
	}

	configDbImportCmd = &cobra.Command{
		Use:   "import DBDIRECTORY IMPORTDIR [GAMEID]",
		Short: "Import per game config data to your config.db for a given gameid or all files found in IMPORTDIR",
		Args:  cobra.MinimumNArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			var (
				importData []GenericDatas
			)

			dbdir := strings.TrimSpace(args[0])
			importdir := strings.TrimSpace(args[1])
			overwrite, err := cmd.Flags().GetBool("overwrite")
			gameid := "*"

			if len(args) > 2 {
				gameid = strings.TrimSpace(args[2])
			}

			err = checkAndMigrateGameDbFull(dbdir)
			if err != nil {
				return err
			}

			importdir = strings.TrimSuffix(importdir, "/")

			global_config, err := api.LoadConfig(dbdir + "/config.yaml")
			if err != nil {
				return err
			}

			err = filepath.Walk(importdir, func(path string, info os.FileInfo, err error) error {
				if err != nil {
					return err
				}

				if info.IsDir() || path != importdir+"/"+info.Name() {
					return nil
				}

				var gds GenericDatas

				reader, err := os.Open(path)
				if err != nil {
					return nil
				}
				defer reader.Close()

				yamlDec := yaml.NewDecoder(reader)
				err = yamlDec.Decode(&gds)
				if err != nil {
					return nil
				}

				if gds.GameId != "" && (gameid == "*" || gds.GameId == gameid) {
					importData = append(importData, gds)
				}
				return nil
			})
			if err != nil {
				return err
			}

			if len(importData) == 0 {
				return fmt.Errorf("No game db data found")
			}

			for _, gds := range importData {

				done, err := importGameDbData(dbdir, &gds, global_config, overwrite)
				if err != nil {
					return err
				}
				if *done {
					fmt.Fprintf(os.Stdout, "Successfully imported game db data for %s\n", gds.GameId)
				} else {
					fmt.Fprintf(os.Stdout, "Skipped importing game db data for %s\n", gds.GameId)
				}
			}

			return nil
		},
	}
)

func listGameIds(filename string, search string) ([]string, error) {
	data := []string{}

	db, err := sql.Open("sqlite", filename)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	stmt, err := db.Prepare("select distinct(gameId) from config where gameId like ? order by gameid")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(search)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var gameId string
		err = rows.Scan(&gameId)
		if err != nil {
			return nil, err
		}
		data = append(data, gameId)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return data, nil
}

func checkIfGameExistsInDB(db *sql.DB, gameid string) (*bool, error) {
	var found bool = false
	stmt, err := db.Prepare("select gameId from config where gameId = ?")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	rows, err := stmt.Query(gameid)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	found = rows.Next()
	return &found, nil
}

func deleteGameFromDb(db *sql.DB, gameid string) error {
	stmt, err := db.Prepare("delete from config where gameId = ?")
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err = stmt.Exec(gameid)
	if err != nil {
		return err
	}
	return nil
}

func deleteRowFromDb(db *sql.DB, data *GenericData) error {
	stmt, err := db.Prepare("delete from config where gameId = ? and context = ? and key = ?")
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err = stmt.Exec(data.GameId, data.Context, data.Key)
	if err != nil {
		return err
	}
	return nil
}

func checkAndMigrateGameDbFull(dbdir string) error {
	filename := dbdir + "/config.db"
	converted, err := checkAndMigrateGameDb(dbdir)
	if err != nil {
		return err
	}

	if *converted {
		err = os.Remove(filename)
		if err != nil {
			return err
		}
		err = os.Rename(dbdir+"/config_new.db", filename)
		if err != nil {
			return err
		}
	}

	return nil
}

func checkAndMigrateGameDb(dbdir string) (*bool, error) {
	var done bool = false

	db, err := sql.Open("sqlite", dbdir+"/config.db")
	if err != nil {
		return nil, err
	}
	defer db.Close()

	stmt, err := db.Prepare("SELECT sql FROM sqlite_master WHERE name = 'config'")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var schem string
		err = rows.Scan(&schem)
		if err != nil {
			return nil, err
		}
		if strings.Index(schem, "\"id\"") != -1 {
			new_db, err := sql.Open("sqlite", dbdir+"/config_new.db")
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error 1\n")
				return nil, err
			}
			defer new_db.Close()

			stmt, err := new_db.Prepare("PRAGMA page_size=512;CREATE TABLE IF NOT EXISTS \"config\" (\"gameId\" TEXT NOT NULL, \"context\" TEXT NOT NULL, \"key\" TEXT NOT NULL, \"value\" BLOB, UNIQUE(gameId, context, key));")
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error 2\n")
				return nil, err
			}
			defer stmt.Close()

			_, err = stmt.Exec()
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error 3\n")
				return nil, err
			}

			read, err := db.Prepare("select gameId, context, key, value from config")
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error 4\n")
				return nil, err
			}
			defer read.Close()

			write, err := new_db.Prepare("insert into config (gameId, context, key, value) values (?, ?, ?, ?)")
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error 5\n")
				return nil, err
			}
			defer write.Close()

			rows, err := read.Query()
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error 6\n")
				return nil, err
			}

			for rows.Next() {
				var gamerow GameDbEntry
				err = rows.Scan(&gamerow.GameId, &gamerow.Context, &gamerow.Key, &gamerow.Value)
				if err != nil {
					fmt.Fprintf(os.Stderr, "Error 7\n")
					return nil, err
				}
				_, err = write.Exec(gamerow.GameId, gamerow.Context, gamerow.Key, gamerow.Value)
				if err != nil {
					fmt.Fprintf(os.Stderr, "Error 8\n")
					return nil, err
				}
			}

			fmt.Printf("Database/table successfully converted!\n")
			done = true
		}
	}

	return &done, nil
}

func importGameDbData(dbdir string, gds *GenericDatas, global_config *api.Configuration, overwrite bool) (*bool, error) {

	var done bool = false

	db, err := sql.Open("sqlite", dbdir+"/config.db")
	if err != nil {
		return nil, err
	}
	defer db.Close()

	if overwrite {
		err = deleteGameFromDb(db, gds.GameId)
		if err != nil {
			return nil, err
		}
	} else {
		found, err := checkIfGameExistsInDB(db, gds.GameId)
		if err != nil {
			return nil, err
		}
		if *found {
			return &done, nil
		}
	}

	meddler.Default = meddler.SQLite

	edata := getExportData(global_config)

	for _, ed := range edata {
		data := getGameDataValue(gds.Data, ed.Context, ed.Key)

		if data != nil && bytes.Compare(data.Value, ed.Global) != 0 {
			data.GameId = gds.GameId

			err = meddler.Insert(db, "config", data)
			if err != nil {
				return nil, err
			}
		}
	}

	done = true
	return &done, nil
}

func exportGameDbData(gameid string, dirname string, filename string) error {
	var (
		writer *os.File
		err    error
		gds    GenericDatas
	)

	db, err := sql.Open("sqlite", dirname+"/config.db")
	if err != nil {
		return err
	}
	defer db.Close()

	meddler.Default = meddler.SQLite

	gds.GameId = gameid
	err = meddler.QueryAll(db, &gds.Data, "select * from config where gameId=?", gds.GameId)
	if err != nil {
		return err
	}

	if len(gds.Data) == 0 {
		return fmt.Errorf("Game ID %s not found in database", gds.GameId)
	}

	global_config, err := api.LoadConfig(dirname + "/config.yaml")
	if err != nil {
		return err
	}

	mergeConfigs(gds.GameId, &gds, global_config)

	writer = os.Stdout
	if filename != "" && filename != "-" {
		writer, err = os.OpenFile(filename, os.O_CREATE|os.O_RDWR|os.O_TRUNC, os.FileMode(0644))
		if err != nil {
			return err
		}
		defer writer.Close()
	}

	yamlEnc := yaml.NewEncoder(writer)
	yamlEnc.SetIndent(2)
	err = yamlEnc.Encode(gds)
	defer yamlEnc.Close()
	if err != nil {
		return err
	}

	return nil
}

func getGameDataValue(data []*GenericData, context string, key string) *GenericData {
	for _, item := range data {
		if context == item.Context && key == item.Key {
			return item
		}
	}
	return nil
}

type exportData struct {
	Context string
	Key     string
	Global  []byte
}

func resolution_to_byte_array(config_resolution string) []byte {
	switch config_resolution {
	case "960p":
		return []byte{'1', 0}
	case "480p":
		return []byte{'2', 0}
	case "VGA":
		return []byte{'3', 0}
	default:
		return []byte{'0', 0}
	}
}

func deinterlacer_to_byte_array(config_deinterlacer string) []byte {
	if config_deinterlacer == "passthru" {
		return []byte{'1', 0}
	}
	return []byte{'0', 0}
}

func string_to_byte_array(s string) []byte {
	return append([]byte(s), 0)
}

func int_to_byte_array(i int) []byte {
	return append([]byte(fmt.Sprintf("%d", i)), 0)
}

func bool_to_byte_array(b bool) []byte {
	if b {
		return []byte{'1', 0}
	}
	return []byte{'0', 0}
}

func getExportData(global_config *api.Configuration) []exportData {
	return []exportData{
		{"resolution", "value", resolution_to_byte_array(global_config.Video.Resolution)},
		{"avs", "colorspace", int_to_byte_array(global_config.Video.Colorspace)},
		{"avs", "colorExpansionMode", int_to_byte_array(global_config.Video.ColorExpansionMode)},
		{"avs", "gammaMode", string_to_byte_array(global_config.Video.GammaMode)},
		{"avs", "forceMode", int_to_byte_array(global_config.Video.ForceMode)},
		{"avs", "autoResetAlignment", bool_to_byte_array(global_config.Video.AutoResetAlignment)},
		{"avs", "deinterlace", deinterlacer_to_byte_array(global_config.Video.Deinterlacer)},
		{"avs", "deinterlacerMode", int_to_byte_array(global_config.Video.DeinterlacerMode)},
		{"avs", "enableHq2x", bool_to_byte_array(global_config.Video.EnableHQ2x)},
		{"avs", "hq2xStage1", bool_to_byte_array(global_config.Video.HQ2xStage1)},
		{"scanlines", "active", bool_to_byte_array(global_config.Scanlines.Active)},
		{"scanlines", "intensity", int_to_byte_array(global_config.Scanlines.Intensity)},
		{"scanlines", "oddeven", bool_to_byte_array(global_config.Scanlines.Odd)},
		{"scanlines", "thickness", bool_to_byte_array(global_config.Scanlines.Thick)},
		{"scanlines", "dopre", bool_to_byte_array(global_config.Scanlines.Dopre)},
		{"scaler", "hscale", global_config.Video.Scaler.Hscale},
		{"scaler", "vscale", global_config.Video.Scaler.Vscale},
		{"capture_adjust", "manual_x", bool_to_byte_array(global_config.CaptureAdjust.ManualX)},
		{"capture_adjust", "manual_y", bool_to_byte_array(global_config.CaptureAdjust.ManualX)},
		{"capture_adjust", "x", int_to_byte_array(global_config.CaptureAdjust.X)},
		{"capture_adjust", "y", int_to_byte_array(global_config.CaptureAdjust.Y)},
	}
}

func mergeConfigs(gameid string, gds *GenericDatas, global_config *api.Configuration) {
	edata := getExportData(global_config)

	for _, ed := range edata {
		value := getGameDataValue(gds.Data, ed.Context, ed.Key)
		if value == nil {
			gds.Data = append(gds.Data, &GenericData{GameId: gameid, Context: ed.Context, Key: ed.Key, Value: ed.Global})
		}
	}
}

func init() {
	cobra.EnableCommandSorting = false
	rootCmd.AddCommand(configCmd)

	configCmd.AddCommand(configDownloadCmd)
	configCmd.AddCommand(configUploadCmd)
	configDownloadCmd.MarkZshCompPositionalArgumentFile(1)
	configUploadCmd.MarkZshCompPositionalArgumentFile(1)

	configCmd.AddCommand(confDbCmd)
	confDbCmd.AddCommand(configDbDownloadCmd)
	confDbCmd.AddCommand(configDbListGameidsCmd)
	confDbCmd.AddCommand(configDbExportCmd)
	confDbCmd.AddCommand(configDbImportCmd)
	confDbCmd.AddCommand(configDbUploadCmd)
	confDbCmd.AddCommand(configDbDeleteCmd)

	configDbDownloadCmd.MarkZshCompPositionalArgumentFile(1)
	configDbListGameidsCmd.MarkZshCompPositionalArgumentFile(1)
	configDbExportCmd.MarkZshCompPositionalArgumentFile(1)
	configDbExportCmd.MarkZshCompPositionalArgumentFile(2)
	configDbImportCmd.MarkZshCompPositionalArgumentFile(1)
	configDbImportCmd.MarkZshCompPositionalArgumentFile(2)
	configDbImportCmd.Flags().Bool("overwrite", false, "Overwrite existing settings")
	configDbUploadCmd.MarkZshCompPositionalArgumentFile(1)
}
