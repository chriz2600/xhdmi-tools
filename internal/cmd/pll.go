package cmd

import (
	"fmt"
	"math/big"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/xhdmi-tools/pkg/api"
)

// gammaCmd represents the gamma command
var (
	pllCmd = &cobra.Command{
		Use:   "pll",
		Short: "XHDMI PLL config helper",
	}

	pllPlanCmd = &cobra.Command{
		Use:   "plan",
		Short: "Creates a PLL frequency plan for XHDMI",
	}

	pllCalcCmd = &cobra.Command{
		Use:   "calc",
		Short: "Calculate pixel clock/frame rate",
	}

	pllCalcPxClkCmd = &cobra.Command{
		Use:   "pixelclock",
		Short: "Calculate output pixel clock frequency for a given raw output timing (in px) and input framerate (in Hz)",
		RunE: func(cmd *cobra.Command, args []string) error {
			_rawwidth, _ := cmd.Flags().GetString("rawwidth")
			_rawheight, _ := cmd.Flags().GetString("rawheight")
			_framerate, _ := cmd.Flags().GetString("framerate")

			rawwidth, err := api.Parse_rat(_rawwidth)
			if err != nil {
				return err
			}
			rawheight, err := api.Parse_rat(_rawheight)
			if err != nil {
				return err
			}
			framerate, err := api.Parse_rat(_framerate)
			if err != nil {
				return err
			}

			p1 := new(big.Rat).Quo(api.From_int(1000000), rawwidth)
			p2 := new(big.Rat).Quo(p1, rawheight)
			p3 := new(big.Rat).Mul(framerate, new(big.Rat).Inv(p2))

			func(i ...int64) { fmt.Printf("%d + %d/%d\n", i[0], i[1], i[2]) }(api.To_abc(p3))

			return nil
		},
	}

	pllCalcFramerateCmd = &cobra.Command{
		Use:   "framerate",
		Short: "Calculate framerate (in Hz) for a given pixel clock (in MHz) and timings (in px)",
		RunE: func(cmd *cobra.Command, args []string) error {
			var pixel *big.Rat

			_rawwidth, _ := cmd.Flags().GetString("rawwidth")
			_rawheight, _ := cmd.Flags().GetString("rawheight")
			_pixel, _ := cmd.Flags().GetString("pixel")
			_pixelclock, _ := cmd.Flags().GetString("pixelclock")

			pixelclock, err := api.Parse_rat(_pixelclock)
			if err != nil {
				return err
			}
			if _pixel != "" {
				pixel, err = api.Parse_rat(_pixel)
				if err != nil {
					return err
				}
			} else {
				if _rawheight == "" || _rawwidth == "" {
					return fmt.Errorf("You have to specify either 'pixel' or 'rawwidth' and 'rawheight'")
				}

				rawwidth, err := api.Parse_rat(_rawwidth)
				if err != nil {
					return err
				}
				rawheight, err := api.Parse_rat(_rawheight)
				if err != nil {
					return err
				}
				pixel = new(big.Rat).Mul(rawwidth, rawheight)
			}

			p1 := new(big.Rat).Mul(api.From_int(1000000), pixelclock)
			p2 := new(big.Rat).Quo(p1, pixel)

			func(i ...int64) { fmt.Printf("%d + %d/%d\n", i[0], i[1], i[2]) }(api.To_abc(p2))

			return nil
		},
	}

	pllCalcEQClkCmd = &cobra.Command{
		Use:   "eq",
		Short: "Calculate equalization pixelclock (in MHz) for a given pixel clock (in MHz)",
		RunE: func(cmd *cobra.Command, args []string) error {
			_clock, _ := cmd.Flags().GetString("clock")
			base, _ := cmd.Flags().GetString("base")
			mode, _ := cmd.Flags().GetString("mode")

			freqBase, err := api.Parse_rat(base)
			if err != nil {
				return err
			}

			clock, err := api.Parse_rat(_clock)
			if err != nil {
				return err
			}

			freqPlan, err := api.NewFrequencyPlan(freqBase, api.XtalCl8pf)
			if err != nil {
				return err
			}

			var (
				p1       *big.Rat
				mult     *big.Rat
				success  = false
				eqValues []*big.Rat
			)
			if mode == "quo" {
				eqValues = []*big.Rat{api.From_abc(0, 10000, 10001), api.From_abc(0, 9999, 10000)}
			} else {
				eqValues = []*big.Rat{api.From_abc(0, 9999, 10000), api.From_abc(0, 10000, 10001)}
			}
			for _, mult = range eqValues {
				if mode == "quo" {
					mult = new(big.Rat).Inv(mult)
				}
				p1 = new(big.Rat).Mul(mult, clock)
				err = freqPlan.SetPll(api.PLLA, p1, true)
				if err == nil {
					success = true
					break
				}
			}
			if !success {
				numerator := int64(9999)
				for {
					mult = api.From_abc(0, numerator, 10000)
					if mode == "quo" {
						mult = new(big.Rat).Inv(mult)
					}
					p1 = new(big.Rat).Mul(mult, clock)
					err = freqPlan.SetPll(api.PLLA, p1, true)
					if err == nil {
						success = true
						break
					}
					numerator = numerator - 1
				}
			}

			if success {
				func(i ...int64) { fmt.Printf("%d + %d/%d\n", i[0], i[1], i[2]) }(api.To_abc(mult))
			} else {
				return fmt.Errorf("Could not find equalization frequency")
			}

			return nil
		},
	}

	pllPlanConvertCmd = &cobra.Command{
		Use:   "convert INPUT TYPE OUTPUT",
		Short: "Convert a PLL config to c header file/verilog task",
		Args:  cobra.ExactArgs(3),
		RunE: func(cmd *cobra.Command, args []string) error {
			inputfilename := strings.TrimSpace(args[0])
			wtype := strings.TrimSpace(args[1])
			outputfilename := strings.TrimSpace(args[2])
			taskname, _ := cmd.Flags().GetString("taskname")
			addTsAndVer, _ := cmd.Flags().GetBool("addtimestamp")

			if wtype != "c" && wtype != "verilog" && wtype != "plltune" {
				return fmt.Errorf("Only 'c', 'verilog' and 'plltune' are allowed TYPEs")
			}

			freqPlan, err := api.LoadPlan(inputfilename)
			if err != nil {
				return err
			}

			if wtype == "c" {
				freqPlan.WriteHeaderFile(outputfilename, false, addTsAndVer)
			} else if wtype == "verilog" {
				freqPlan.WriteVerilogInclude(taskname, outputfilename, true, addTsAndVer)
			} else if wtype == "plltune" {
				freqPlan.WritePLLTune(taskname, outputfilename)
			}

			return nil
		},
	}

	pllPlanCreateCmd = &cobra.Command{
		Use:   "create FILE",
		Short: "Create a new PLL config",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			filename := strings.TrimSpace(args[0])
			base, _ := cmd.Flags().GetString("base")

			freqBase, err := api.Parse_rat(base)
			if err != nil {
				return err
			}
			psx, _ := cmd.Flags().GetString("psx")
			freqPS1, err := api.Parse_rat(psx)
			if err != nil {
				return err
			}
			hdmi, _ := cmd.Flags().GetString("hdmi")
			freqHdmi, err := api.Parse_rat(hdmi)
			if err != nil {
				return err
			}
			sdram, _ := cmd.Flags().GetString("sdram")
			freqSdram, err := api.Parse_rat(sdram)
			if err != nil {
				return err
			}

			freqPlan, err := api.NewFrequencyPlan(freqBase, api.XtalCl8pf)
			if err != nil {
				return err
			}

			err = freqPlan.SetPll(api.PLLA, freqHdmi, true)
			if err != nil {
				return err
			}
			err = freqPlan.SetPll(api.PLLB, freqSdram, true)
			if err != nil {
				return err
			}

			err = freqPlan.SetClock(api.Clock1, api.PLLB, freqPS1, api.IDrv8ma)
			if err != nil {
				return err
			}
			err = freqPlan.SetClock(api.Clock4, api.PLLA, freqHdmi, api.IDrv8ma)
			if err != nil {
				return err
			}
			err = freqPlan.SetClock(api.Clock5, api.PLLA, freqHdmi, api.IDrv8ma)
			if err != nil {
				return err
			}
			err = freqPlan.SetClock(api.Clock7, api.PLLB, freqSdram, api.IDrv8ma)
			if err != nil {
				return err
			}

			err = api.SavePlan(freqPlan, filename)
			if err != nil {
				return err
			}
			return nil
		},
	}
)

func init() {
	cobra.EnableCommandSorting = false
	rootCmd.AddCommand(pllCmd)
	pllCmd.AddCommand(pllCalcCmd)
	pllCmd.AddCommand(pllPlanCmd)

	pllCalcCmd.AddCommand(pllCalcPxClkCmd)
	pllCalcCmd.AddCommand(pllCalcFramerateCmd)
	pllCalcCmd.AddCommand(pllCalcEQClkCmd)

	pllPlanCmd.AddCommand(pllPlanCreateCmd)
	pllPlanCmd.AddCommand(pllPlanConvertCmd)

	pllPlanCreateCmd.Flags().String("base", "25", "Base osc clock frequency in MHz")
	pllPlanCreateCmd.Flags().String("psx", "53 + 61/88", "PS1 GPU clock frequency in MHz")
	pllPlanCreateCmd.Flags().String("hdmi", "148 + 238/3419", "sdram clock frequency in MHz")
	pllPlanCreateCmd.Flags().String("sdram", "108", "hdmi clock frequency in MHz")
	pllPlanCreateCmd.MarkZshCompPositionalArgumentFile(1)

	pllPlanConvertCmd.Flags().String("taskname", "default", "Taskname for verilog output")
	pllPlanConvertCmd.Flags().Bool("addtimestamp", false, "Add timestamp to output file header")
	pllPlanConvertCmd.MarkZshCompPositionalArgumentFile(1)
	pllPlanConvertCmd.MarkZshCompPositionalArgumentWords(2, "c", "verilog", "plltune")
	pllPlanConvertCmd.MarkZshCompPositionalArgumentFile(3)

	pllCalcEQClkCmd.Flags().String("taskname", "default", "Name for generated code output")
	pllCalcEQClkCmd.Flags().String("clock", "", "Refrence clock frequency in MHz")
	pllCalcEQClkCmd.Flags().String("base", "25", "Base osc clock frequency in MHz")
	pllCalcEQClkCmd.Flags().String("mode", "mul", "Mode: 'mul' or 'quo'")
	pllCalcEQClkCmd.MarkFlagRequired("clock")

	pllCalcPxClkCmd.Flags().String("framerate", "", "framerate of input signal (in Hz)")
	pllCalcPxClkCmd.Flags().String("rawwidth", "", "raw width of output video signal (in px)")
	pllCalcPxClkCmd.Flags().String("rawheight", "", "raw height of output video signal (in px)")
	pllCalcPxClkCmd.MarkFlagRequired("framerate")
	pllCalcPxClkCmd.MarkFlagRequired("rawwidth")
	pllCalcPxClkCmd.MarkFlagRequired("rawheight")

	pllCalcFramerateCmd.Flags().String("rawwidth", "", "raw width of output video signal (in px)")
	pllCalcFramerateCmd.Flags().String("rawheight", "", "raw height of output video signal (in px)")
	pllCalcFramerateCmd.Flags().String("pixel", "", "number of pixel per frame (in px)")
	pllCalcFramerateCmd.Flags().String("pixelclock", "", "pixelclock (in MHz)")
	pllCalcFramerateCmd.MarkFlagRequired("pixelclock")
}
