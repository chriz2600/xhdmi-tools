# Installers

If you are a PS1Digital installer `xhdmi-tools` might help you.

## Step 0

***This step only has to be done once!***

Create a console configuration:

1) `dut`(device under test): This is your testing setup containing WiFi SSID/password etc.
    ```
    xhdmi-tools console add dut --hostname <static-ip> --user ps1digital --password <my-user-password>
    ```
2) Prepare two config files:

    ***[Config parameter documentation](https://psx.i74.de/config/)***
    <br/>
    ***[Advanced Video Settings documentation](https://psx.i74.de/avs/)***

    - [`factory.config`](https://gitlab.com/chriz2600/xhdmi-tools/-/blob/master/example/factory.config)

        This is the default factory config file. This will be used to restore factory state after testing/firmware update. No changes are needed for this file.

    - [`dut.config`](https://gitlab.com/chriz2600/xhdmi-tools/-/blob/master/example/dut.config)

        This is the config file containing your WiFi SSID/password and static ip address settings. The static ip is the same as `<static-ip>` from the `xhdmi-tools console add dut` command above. Create your version of this file.


## Step 1

- Start PS1Digital and use `L2` + `start` + `square` + `triangle` + `X` + `O` to enter IAP (Installer Access Point) mode. A warning OSD screen will be shown.

  This will start an access point with the following settings:

  ```
  WiFi SSID:     PS1Digital-Install
  WiFi Password: installme!
  HTTP Username: please
  HTTP Password: installme!
  IP Address:    192.168.4.1
  ```

- Connect to this access point.

## Step 2

Upload your `dut.config` to the console, using:

```
xhdmi-tools console use iap-mode
xhdmi-tools config upload dut.config
xhdmi-tools console reset
```

## Step 3

Update firmware, etc

### Firmware update (using `xhdmi-tools`)

Switch the currently used config from `factory` to `dut`:
```
xhdmi-tools console use dut
```

Run:
```
xhdmi-tools console fw auto
```
to download, flash and finally restart the console with lastest firmware for configured version.


## Step 4

Restore clean factory config:

``` 
xhdmi-tools config upload factory.config
xhdmi-tools console reset
```
