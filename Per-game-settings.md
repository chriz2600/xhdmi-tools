# Per game settings support

`xhdmi-tools` (version >= `1.7.2`) supports downloading, exporting, importing and uploading per game settings from/to your PS1.

This allows sharing per game settings with other users.

The console's config database only stores values differing from the global configuration. To allow all settings to be reproduced perfectly on a different console having different global settings, the exported file format contains all settings active for that game, including the global settings of the exporting console.

When importing, all settings different from the target console's global configuration will be written to the config database.

## Prerequisites

Refer to [xhdmi-tools setup](https://gitlab.com/chriz2600/xhdmi-tools#setup) on how to set up `xhdmi-tools`.

## Get config data from the console

Download the config db and the global settings, using this command:

```
xhdmi-tools config db download DB_DIRECTORY
```

- `DB_DIRECTORY`: directory of your choice to hold the downloaded `config.db` and `config.yaml`

*Hint: After changing settings on your console, you have to download the files again, so best practice should be to download them every time you want to `export` or `import` config data.*

## Export config data for sharing

```
xhdmi-tools config db export DB_DIRECTORY EXPORT_DIR [GAMEID]
```

- `DB_DIRECTORY`: directory holding the previously downloaded config data

- `EXPORT_DIR`: target directory where exported game data is saved, the directory is created, if it doesn't exist. Each game id will go into one file named `GAME_ID.pgsd`

- `GAMEID`: ***optional***, game id of the game to export. If missing, all games for which settings are available are exported

Example of files in `EXPORT_DIR` after exporting:

```shell
# ls -1 gamedb/exportdir
PSX.EXE.pgsd
SCES-02873.pgsd
SLUS-00874.pgsd
SLUS-00922.pgsd
```

## Import config data

```
xhdmi-tools config db import DB_DIRECTORY IMPORT_DIR [GAMEID] [--overwrite]
```

- `DB_DIRECTORY`: directory holding the previously downloaded config data

- `IMPORT_DIR`: source directory from where settings are imported (put `.pgsd` files there). The actual file name doesn't matter, all files in that directory are parsed for valid config data.

- `GAMEID`: ***optional***, game id of the game to import. If missing, data from all valid files in `IMPORT_DIR` is imported

- `--overwrite`: ***optional***, without this option, data import for games already in `config.db` is skipped. If the flag is present, the data in `config.db` is overwritten.

## Upload config data to the console

```
xhdmi-tools config db upload DB_DIRECTORY
```

- `DB_DIRECTORY`: directory holding the previously downloaded (and now modified from the import) config data

*Hint: Please make sure to NOT upload the database, while it's in use, which is during starting a game from your ODE's loader/menu.*