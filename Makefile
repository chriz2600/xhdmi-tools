.DEFAULT_GOAL := all

.PHONY: build run dist clean

build:
	CGO_ENABLED=0 go build -ldflags="-X 'gitlab.com/xhdmi-tools/pkg/api.Date=$$(date)'" -o bin/xhdmi-tools cmd/xhdmi-tools/main.go

run: build
	./bin/xhdmi-tools

dist:
	echo "Creating binaries"
	GOOS=windows GOARCH=386 go build -o bin/xhdmi-tools-windows cmd/xhdmi-tools/main.go
	GOOS=linux GOARCH=amd64 go build -o bin/xhdmi-tools-linux cmd/xhdmi-tools/main.go
	GOOS=darwin GOARCH=amd64 go build -o bin/xhdmi-tools-macos-amd64 cmd/xhdmi-tools/main.go
	GOOS=darwin GOARCH=arm64 go build -o bin/xhdmi-tools-macos-arm64 cmd/xhdmi-tools/main.go

clean:
	rm bin/xhdmi-tools*

all: run
